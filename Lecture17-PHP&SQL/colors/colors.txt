<?php
$dbname = $user = $pass = 'lec17';

try {
  $dbconn = new PDO('mysql:host=localhost;dbname='.$dbname, $user, $pass);
}
catch (Exception $e) {
  echo "Error: ";
  echo $e->getMessage();
}
?>
<!doctype html>
<html>
<head>
  <title>Colors!</title>
</head>
<body>
  <h1>Colors:</h1>
  <ul>
  <?php 
  $stmt = $dbconn->query("SELECT * FROM lec17");
  foreach ($stmt as $row) { ?>
    <li><?php echo htmlentities($row['colorname']) ?> - <?php echo htmlentities($row['hex']) ?></li>
  <?php } ?>
  </ul>
</body>
</html>
