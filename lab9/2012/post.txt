<?php
	if (isset($_POST['post_text']) || (isset($_GET['pid']) && is_numeric($_GET['pid'])) ) {
		$dbname = 'lab9';
		$user = $pass = 'root';
		try {	
			// Create a new PDO object		
			$dbconn = new PDO('mysql:host=localhost;dbname='.$dbname, $user, $pass);
			
			if(isset($_POST['update'])) {
				$sql = 'UPDATE posts SET post = :post WHERE pid = :pid';
				$stmt = $dbconn->prepare($sql);
				$stmt->execute( array(':post' => $_POST['post_text'], ':pid' => $_POST['pid']));
				header('Location:list.php');
			}
			elseif (isset($_POST['submit'])) {
				$sql = 'INSERT INTO posts (post) VALUES (:post)';
				$stmt = $dbconn->prepare($sql);
				$stmt->execute(array(':post' => $_POST['post_text']));
				header('Location:list.php');
			}
			else {
				$sql = 'SELECT * FROM posts WHERE pid = :pid';
				$stmt = $dbconn->prepare($sql);
				$stmt->execute(array(':pid' => $_GET['pid']));
				$result = $stmt->fetch(PDO::FETCH_ASSOC);
			}
		}
		catch (Exception $e) {
			echo "Error: ";
			echo $e->getMessage();
		}
	}
?>
<html>
	<head>
    	<title>Web Systems Lab 9</title>
        <!-- Just some styling to make the example look nicer. Might be useful for future projects -->
        <link rel="stylesheet" type="text/css" href="foundation/stylesheets/app.css" />
        <link rel="stylesheet" type="text/css" href="foundation/stylesheets/foundation.css" />
        <link rel="stylesheet" type="text/css" href="foundation/stylesheets/ie.css" />
    </head>
    <body>
    	<div class="container">
            <a href="list.php">List all Posts</a>
        	<?php
			// If we're doing an UPDATE
			if(isset($_GET['pid']) && is_numeric($_GET['pid'])) { 
				echo '<h2>Update Post #'.htmlentities($_GET['pid']).'</h2>';
			?>
            	<form method="post" action="post.php">
            <?php
                echo '
				<input type="hidden" name="pid" value="'.$_GET['pid'].'" />
                <input type="text" name="post_text" value="'.htmlentities($result['post']).'" />
                <input type="submit" name="update" value="Update" />
                ';
            ?>
            	</form>
			<?php
            }
            // If we CREATE a new post
            else {
                echo '<h2>Make a New Post!</h2>';
            ?>
                <form method="post" action="post.php">
				<?php
                    echo'
                    <input type="text" name="post_text" />
                    <input type="submit" name="submit" value="Post" />
                    ';
                ?>
                </form>
            <?php
            }
            ?>
        </div>
        <!-- Used for styling purposes -->
        <script src="foundation/javascripts/jquery.min.js" type="text/javascript"></script>
        <script src="foundation/javascripts/app.js" type="text/javascript"></script>
        <script src="foundation/javascripts/foundation.js" type="text/javascript"></script>
        <script src="foundation/javascripts/modernizr.foundation.js" type="text/javascript"></script>
    </body>
</html>