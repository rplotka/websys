<?php 

require_once 'config.php';
require_once 'connect.php';

function insert() {
   try {
      $conn = DBconnect($GLOBALS['DB_HOST'], $GLOBALS['DB_NAME'], $GLOBALS['DB_USERNAME'], $GLOBALS['DB_PASSWORD']);

      $newrcd = "INSERT INTO courses (crn, prefix, num, title, section, year) 
         VALUES (2100, 'ITWS', '2100', 'Web Sys I','Fall','2013') ON DUPLICATE KEY UPDATE id=id;";
      $conn->exec($newrcd);
      printf(nl2br("Course inserted\n"));

      $newrcd = "INSERT INTO students (rin, fname, lname, addr1, addr2, city, state, zip, zip4)
         VALUES   (0, 'John', 'Jones','1313 Mockingbird Lane',' ','Anytown', 'NY','12180','1234'),
                  (1, 'Fred', 'Doe','1313 Mockingbird Lane',' ','Anytown', 'NY','12180','1234'),
                  (2, 'Mary', 'Dolan','1313 Mockingbird Lane',' ','Anytown', 'NY','12180','1234'),
                  (3, 'Jane', 'Smith','1313 Mockingbird Lane',' ','Anytown', 'NY','12180','1234'),
                  (4, 'Jill', 'Told','1313 Mockingbird Lane',' ','Anytown', 'NY','12180','1234'),
                  (5, 'Matt', 'Damon','1313 Mockingbird Lane',' ','Anytown', 'NY','12180','1234'),
                  (6, 'Jack', 'Kennedy','1313 Mockingbird Lane',' ','Anytown', 'NY','12180','1234'),
                  (7, 'Katt', 'Kit','1313 Mockingbird Lane',' ','Anytown', 'NY','12180','1234'),
                  (8, 'Paul', 'Dalili','1313 Mockingbird Lane',' ','Anytown', 'NY','12180','1234'),
                  (9, 'King', 'Royal','1313 Mockingbird Lane',' ','Anytown', 'NY','12180','1234') ON DUPLICATE KEY UPDATE id=id;                  
                  ";
      $conn->exec($newrcd);
      print(nl2br("Students inserted\n"));

      $newrcd = "INSERT INTO grades (crn, rin, grade)
         VALUES   (2100, 0, 95),
                  (2100, 1, 45),
                  (2100, 2, 80),
                  (2100, 3, 10),
                  (2100, 4, 99),
                  (2100, 5, 92),
                  (2100, 6, 65),
                  (2100, 7, 55),
                  (2100, 8, 90),
                  (2100, 9, 89) ON DUPLICATE KEY UPDATE id=id;
                  ";
      $conn->exec($newrcd);
      printf(nl2br("Grades inserted\n"));
   } catch(PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
   }
}
 ?>

