<?php 
   function qry($option) {

   try {
      $conn = DBconnect($GLOBALS['DB_HOST'], $GLOBALS['DB_NAME'], $GLOBALS['DB_USERNAME'], $GLOBALS['DB_PASSWORD']);

      switch($option) {
         case 1: {
            $qryslt = "SELECT * FROM students ORDER BY lname";
            $result = $conn->query($qryslt);
            print(nl2br("RIN, Student\n\n"));
            foreach ($result as $row) {
               $student = sprintf("%d, %s %s\n", $row['rin'], $row['fname'],$row['lname']);
               print(nl2br($student));
            }
            break;
         }
         case 2: {
            // minimum required
            // SELECT s.lname, g.grade
            // FROM grades g
            // LEFT JOIN students s ON g.rin = s.rin

            $qryslt="
                     SELECT s.lname, g.grade, c.num, c.title
                     FROM grades g
                     LEFT JOIN students s ON g.rin = s.rin
                     LEFT JOIN courses c ON g.crn=c.crn
                   ;";

            $a=$b=$c=$d=$f=0;
            $result = $conn->query($qryslt);
            foreach ($result as $row) {
               if ($row['grade']>=90) {
                  $a++;
               }
               if ($row['grade']>=80 && $row['grade']<90) {
                  $b++;
               }
               if ($row['grade']>=70 && $row['grade']<80) {
                  $c++;
               }
               if ($row['grade']>=65 && $row['grade']<70) {
                  $d++;
               }
               if ($row['grade']<65) {
                  $f++;
               }
            }
            $output = sprintf("The number of grades were;\nA=%-2d\nB=%-2d\nC=%-2d\nD=%-2d\nF=%-2d\n",$a,$b,$c,$d,$f);
            print(nl2br($output));
            break;
         };
      };


   } catch(PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
   }
}
 ?>