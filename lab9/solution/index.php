 <!doctype html>
 <html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Just some styling to make the example look nicer. Might be useful for future projects -->
        <link rel="stylesheet" type="text/css" href="foundation/css/foundation.css" />
        <title>Lab 9 solution</title>
    </head>

    <body>
        <div class="row">
            <div class="large-12 columns">
                <h2>Web Sys I</h2>
                <p>Lab 9 solution with some Foundation styling 4.3.2.</p>
                <hr />
            </div>
        </div>

        <div class="row">
            <div class="large-12 columns">
                <?php
                    require_once 'config.php';   
                    require 'install.php';
                    require 'insert.php';
                    require 'query.php';
                    session_start();

                    if(isset($_POST["install"])) {
                       install();
                    }
                    if(isset($_POST["load"])){
                       insert();
                    }

                    if(isset($_POST["student"])) {
                        qry(1);
                    }
                    if(isset($_POST["grade"])) {
                        qry(2);
                    }
                ?>
                <hr />
            </div>
        </div>

        <div class="row">
            <div class="small-4 columns">
                <div class="button-bar">
                    <form action ="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
                        <ul class="button-group round even-2">
                            <li><input class="small button" type = "submit" value = "student" name = "student" /></li>
                            <li><input class="small button" type = "submit" value = "grade" name = "grade" /></li>
                        </ul>
                        <ul class="button-group round even-2">
                            <li><input class="tiny button alert" type = "submit" value = "install" name = "install" />  </li>
                            <li><input class="tiny button alert" type = "submit" value = "load" name = "load" />        </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>

        <!-- Used for styling purposes -->
        <script src="foundation/js/vendor/jquery.js" type="text/javascript"></script>
        <script src="foundation/js/foundation.min.js" type="text/javascript"></script>
        <script src="foundation/js/vendor/custom.modernizr.js" type="text/javascript"></script>
     </body>
 </html>