<?php 
// session_start();

require_once 'config.php';
require_once 'connect.php';

function install() {
   try {
      $conn = DBconnect($GLOBALS['DB_HOST'], '', $GLOBALS['DB_USERNAME'], $GLOBALS['DB_PASSWORD']);
      echo 'Server Connected'.'<br/>';

// Prepare CREATE DATABASE statement
      $pstmt = $conn->prepare('CREATE DATABASE IF NOT EXISTS websyslab9');
      $status = $pstmt->execute();
      echo 'Create done. Status=' . $status.'<br/>';

      $sql = "USE websyslab9";
      $status = $conn->exec($sql);
      printf("Using. Status=%s\n",$status);

      // CREATE TABLE
      $sql = "CREATE TABLE IF NOT EXISTS `students` (
      `id` int(10) NOT NULL AUTO_INCREMENT,   
      `rin` int(9) NOT NULL UNIQUE,
      `fname` varchar(100) NOT NULL,
      `lname` varchar(100) NOT NULL,
      `addr1` varchar(255) NOT NULL,
      `addr2` varchar(255),
      `city`  varchar(255),
      `state` varchar(255),
      `zip`   int(5),
      `zip4`  int(4),
      PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";

      // We use trim() to remove all the whitespace from $sql
      $sql = trim($sql);
 
      // Execute the prepared statement - Create our table
      $status = $conn->exec($sql);


      echo 'Created students table. Status:'.$status.'<br/>';

      // CREATE TABLE
      $sql = "CREATE TABLE IF NOT EXISTS `courses` (
      `id` int(10) NOT NULL AUTO_INCREMENT,  
      `crn` int(11) NOT NULL UNIQUE,
      `prefix` varchar(4) NOT NULL,
      `num` smallint(4) NOT NULL,
      `title` varchar(255) NOT NULL,
      `section` varchar(255) NOT NULL,
      `year` year(4) NOT NULL,
      PRIMARY KEY (`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
      $sql = trim($sql);
      $status = $conn->exec($sql);
      echo 'Created courses table. Status:'.$status.'<br/>';

      // CREATE TABLE
      $sql = "CREATE TABLE IF NOT EXISTS `grades` (
      `id` int(10) NOT NULL AUTO_INCREMENT,
      `crn` int(11) NOT NULL,
      `rin` int(9) NOT NULL,
      `grade` int(3) NOT NULL,
      PRIMARY KEY (`id`),
      FOREIGN KEY (crn) REFERENCES courses(crn),
      FOREIGN KEY (rin) REFERENCES students(rin)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
      $sql = trim($sql);
      $status = $conn->exec($sql);
      echo 'Created grades table. Status:'.$status.'<br/>';
   } catch(PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
   }


}
 ?>