<?php 


   require 'config.php';


   try {
      $conn = new PDO('mysql:host=localhost;dbname=websys_shop', $config['DB_USERNAME'], 
         $config['DB_PASSWORD']);
      if ($conn) {
         echo "Connected!";
         $conn = null;
      }
   } catch(PDOException $e) {
      echo "DB not found!!\n";
      echo "creating a new instance....\n";
      try {
         $conn = new PDO("mysql:host=localhost", $config['DB_USERNAME'], 
            $config['DB_PASSWORD']);
         $conn->exec("CREATE DATABASE websys_shop;") 
         or die(print_r($conn->errorInfo(), true));
         if ($conn) {
            echo "Successfully created!";
            $conn = null;
         }
      } catch(PDOException $e) {
         die('ERROR: ' . $e->getMessage());   
      }
   }
 ?>