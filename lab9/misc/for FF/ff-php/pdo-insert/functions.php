<?php 


   require 'config.php';

   try {
      $conn = new PDO('mysql:host=localhost;dbname=websys_shop', $config['DB_USERNAME'], $config['DB_PASSWORD']);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $conn->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);

      $test_sql = "select 1 from customers";
      $test_stmt = $conn->prepare($test_sql);
      $test_stmt->execute();
      
      insert_record($conn,1,"John","Smith");
   } catch(PDOException $e) {
       //create table

        echo "Table doesn't exist.<br/>";
        echo "Creating new table...<br/>";
        $tbl_sql =  "CREATE TABLE customers( ".
                    "customer_id INT NOT NULL AUTO_INCREMENT, ".
                    "fname VARCHAR(40) NOT NULL, ".
                    "lname VARCHAR(40) NOT NULL, ".
                    "creation_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, ".
                    "PRIMARY KEY ( customer_id )); ";
        $conn->exec($tbl_sql);
        echo "Table created successfully.<br/>";
        insert_record($conn,1,"John","Smith");
   }
   function insert_record($conn,$iid,$ifname,$ilname) {
    if ($conn) {
      try {
        $pstmt = $conn->prepare("INSERT INTO customers (customer_id,fname,lname) VALUES (:id, :fname, :lname)");
        $pstmt->bindParam(':id', $iid);
        $pstmt->bindParam(':fname', $ifname);
        $pstmt->bindParam(':lname', $ilname);
        $pstmt->execute();
        echo "Inserted record.";
      }catch(PDOException $e){
        die('INSERT ERROR: ' . $e->getMessage());      
      }
    } else {
      echo "No connection for inserting a record.";
    }
   }
 ?>