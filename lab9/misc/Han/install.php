<?php

$host = "localhost";
$root = "root";
$root_password="";

$user = "han";
$pass = "pw";
$db = "websyslab9";

	try {
		$dbh = new PDO("mysql:host = $host", $root, $root_password);
		$dbh->exec("create database if not exists $db;")
		or die (print_r($dbh->errorInfo(), true));

		echo "database created...";

		// $lab9db = new PDO("mysql:host = '$host'; dbname = '$db'", $root, $root_password);

		// $isCon = mysql_select_db('websyslab9');

		$dbh->exec("use websyslab9");

		echo "connected to database...";
			
		$dbh->exec("DROP TABLE IF EXISTS courses;
			CREATE TABLE courses(crn int(11), preifx varchar(4), number smallint(4) not null, title varchar(255), section int, year int, primary key(crn));");
		// or die (print_r($dbh->errorInfo(), true));

		echo "courses table created...";

		$dbh->exec("DROP TABLE IF EXISTS students;
			CREATE TABLE students(rin int(9), first_name varchar(100) not null, last_name varchar(100) not null, Address1 varchar(255), Address2 varchar(225), City varchar(100), State varchar(100), Zip int, Zip4 int, primary key(rin));");
		// or die (print_r($dbh->errorInfo(), true));

	echo "sudents table created...";

		$dbh->exec("DROP TABLE IF EXISTS grades;
			CREATE TABLE grades(id int, crn int(11), rin int(9), primary key (id), grade int(3) not null, constraint foreign key(crn) references courses(crn), constraint foreign key (rin) references students(rin));");
		// or die (print_r($dbh->errorInfo(), true))

		echo "grades table created...";
		
	} catch (PDOException $e){
		die("DB ERROR: ".$e->getMessage());
	}

?>