

$(document).ready(function() {
  $.ajax({
    type: "GET",
    url: "menuItems.xml",
    dataType: "xml",
    success: function(responseData, status) {
      var lectureli = '';
      var labsli='';
      var extrasli='';
      $(responseData).find("menuItem").each(function() {

      if ($(this).find("menuCat").text()=="lecture") {
        lectureli += '<li>';
        lectureli += '<a href="#">' + $(this).find("menuName").text() + '</a>';
        lectureli += '<ul>';
        lectureli += '<li>' + '<a href="' + $(this).find("menuURL").text();
        lectureli += '">' + $(this).find("menuDesc").text() + '</a></li>';
        lectureli += '</ul>';
        lectureli += '</li>';
      }
      if ($(this).find("menuCat").text()=="lab") {
         labsli += '<li>';
         labsli += '<a href="#">' + $(this).find("menuName").text() + '</a>';
         labsli += '<ul>';
         labsli += '<li>' + '<a href="' + $(this).find("menuURL").text();
         if ($(this).find("menuName").text() == 'Lab 5') {
          labsli += '" id="swx" onmouseover="switchText();" onmouseout="switchText();';
         }
         labsli += '">' + $(this).find("menuDesc").text() + '</a></li>';
         labsli += '</ul>';
         labsli += '</li>';
      }
      if ($(this).find("menuCat").text()=="extra") {
         extrasli += '<li>';
         extrasli += '<a href="#">' + $(this).find("menuName").text() + '</a>';
         extrasli += '<ul>';
         extrasli += '<li>' + '<a href="' + $(this).find("menuURL").text();
         extrasli += '">' + $(this).find("menuDesc").text() + '</a></li>';
         extrasli += '</ul>';
         extrasli += '</li>';
      }
      $("#accordion-1").html(lectureli).accordion("refresh");
      $("#accordion-2").html(labsli).accordion("refresh");
      $("#accordion-3").html(extrasli).accordion("refresh");

         // $("#tabs").html(menuli).tabs("refresh");
      });

    }, error: function(msg) {
      alert("There was a problem: " + msg.status + " " + msg.statusText);
    }
  });
});

$(function() {
  $("#accordion-1").accordion();
});
$(function() {
  $("#accordion-2").accordion();
});
$(function() {
  $("#accordion-3").accordion();
});

$(function() {
  $("#tabs").tabs();
});

function switchText() {
  var id = $("[id^=swx").attr("id");

  if(id=="swx") {
    $("#swx").text("Quiz 1").removeAttr("id").attr("id", "swxback");
  } else {
    $("#swxback").text("Lab 5").removeAttr("id").attr("id", "swx");
  }
}


