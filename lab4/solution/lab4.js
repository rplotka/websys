$('#coverart').click(function(e) {

  var i=0;

  $.ajax({
    type: "GET",
    url: "lab4.json",
    dataType: "json",
    success: function(responseData, status) {

      var title = '';
      var album = '';
      var artist = '';
      var date = '';
      var genre = '';

      $.each(responseData.song, function(i, item) {

        // body...
           title += '<li>' + item.Trackname + "</li>";
           album += '<li>' + item.Album + "</li>";
           artist += '<li>' + item.Artist + "</li>";
           date += '<li>' + item.Releasedate + "</li>";

          // genre += '<ul class='genrecontainer'>';
          genre += '<ul class="genrecontainer">';
          $(item.Genres).each(function(i, gen) {
            // alert(i+')' + gen.Genre);
            genre += '<li>' + gen.Genre + "</li>";
          });
          genre += '</ul>';
// alert(genre);


// we want to replace the first a and img tags with real data, then want to clone and update the rest.   
// We want 1 item for each entry in the json file, not each entry plos the one that was already there 
// to show the nocover image.

        if(i==0) {
          $("a").last().attr("href",item.Artistsite).text(item.Artistsite);
          $("img").last().attr("src",item.AlbumURL);
        } else {
          $(".siteli").last().clone().appendTo("#site");
          $("a").last().attr("href",item.Artistsite).text(item.Artistsite);
          $(".coverli").last().clone().appendTo("#coverart");
          $("img").last().attr("src",item.AlbumURL);
        }
      });

    $("#title").html(title);
    $("#album").html(album);
    $("#artist").html(artist);
    $("#date").html(date);
    $("#genres").html(genre);
    $(".footer").html('<li>End of List</li>');


    }, error: function(msg) {
      alert("There was a problem: " + msg.status + " " + msg.statusText);
    }
  });
});


      // // We'll make this request in the onclick event handler for #trigger.
      // $('#coverart').click(function(e) {
      //   // Make a GET request to the given URL for a JSON source.
      //   // In the callback, we'll manipulate the response. This callback accepts
      //   // three arguments: the data, the status, and a jQuery-enhanced
      //   // XMLHttpRequest object. For now, we just need the data.
      //   $.getJSON('lab4.json', function(data) {
      //     // Before the callback, $.parseJSON() has already been called.
      //     // This means it has already been converted from JSON to a JavaScript
      //     // object for us to manipulate.
 
      //     // For the sake of exploring JSON, let's have some fun with it now
      //     // that it's a JavaScript object. We'll iterate through our array of
      //     // individuals with the global $.each() iterator function.

      //     // Recall that data.people is an array.

      //     $.each(data.song, function(key, val) {
      //       alert(val.Artist);
      //     });




      //     // $.each(data.title, function(key, val) {
      //     //   // We can access JSON parsed with parseJSON() just like any other
      //     //   // object's properties.
        
      //     //   alert("You, " + val.name + ", what is your profession?");
      //     //   alert("I'm a " + val.profession);
      //     // });
      //   });
      // });