# readme.txt

# Parker Sikand
# Web Systems Development
# Homework 2: Hexed

1) What are the advantages to writing a jQuery plugin over regular JavaScript that uses jQuery?

	By packaging our code in a jquery plugin, it is more modular and more portable. By refining our code inside of this plugin, we can make sure that it will function robustly, no matter how or where the plugin is used.

2) Explain how your jQuery plugin adheres to best practices in JavaScript and jQuery development.

	Well, it really doesn't. Since all content is generated from the javascript, a browser or device without javascript support would not be able to play the game. They would just get a blank page. One positive aspect of my code is that it does separate javascript event handling from the markup, ie there are no "onclick='stuff()'" lines anywhere. All of the event handling is bound and maintained in the javascript. My code also stores any re-used elements in variables. While this makes for more source lines and more memory usage, there are no redundant selectors which should improve front end performance.

3) Does anything prevent you from POSTing high scores to a server on a different domain? If so, what? If not, how would we go about it (written explanation/pseudocode is fine here)?
	
	Yes, basic browser security as outlined by the W3C prevents our javascript from making any calls to foreign domains. The reason for this is to prevent Cross Site Scripting (XSS) attacks. We can work around this by creating a proxy on the hosting machine that redirects the call to another machine.

4) Now that you've used Web Storage, what other information would you store there in other Web-based applications? Is there any information you wouldn't store?

	Web storage seems like a good place to store website-specific front-end settings, as the storage spaces are probably emptied less frequently than cookies. There is also no need to transmit front-end settings as a cookie back and forth to the server. I wouldn't store any passwords or other sensitive data, as web storage is by default not encrypted. I would also not store any data that the application depends on to function, as some users may wish to frequently clear their local storage.
