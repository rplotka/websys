//class to hold color information and make a hex string
function Color (r,g,b){
  this.r=r;
  this.g=g;
  this.b=b;
  this.hex = function() {
    var rs = this.r.toString(16);
    var gs = this.g.toString(16);
    var bs = this.b.toString(16);
    if(rs.length==1) rs = "0"+rs;
    if(gs.length==1) gs = "0"+gs;
    if(bs.length==1) bs = "0"+bs;
    return "#"+rs+gs+bs;
  }
};

//class to hold score info for high scores
function Score(n,d,t,s,time) {
  this.name = n;
  this.difficulty = d;
  this.turns = t;
  this.score = s;
  this.time = time;
};

//Object to manage the high scores
var HexedHighScores = {
  scores: [],
  init: function() {
      for(var i=0; i<window.localStorage.length; i++) {
        var key = window.localStorage.key(i);
        if(/HexedScore:\d+/.test(key)){
          this.scores.push(JSON.parse(window.localStorage.getItem(key)));
        }
      }

  },
  addScore: function(score) {
    localStorage.setItem("HexedScore:"+score.time, JSON.stringify(score));
  },
  makeTable: function() {
    if(this.scores.length<1) this.init(); //if scores is empty, read them from localStorage
    //if its still empty, there are none
    if(this.scores.length<1) return "<p>There are no saved high scores at this time</p>";
    var tbl = $("<table id='hexed-score-table'></table>");
    tbl.append("<tr>\
                <th>Player</th>\
		<th>Difficulty</th>\
		<th>Turns</th>\
		<th>Score</th>\
		<th>Time</th></tr>")
    for(var i=0; i<this.scores.length; i++) {
      var tr = $("<tr></tr>");
      var s = this.scores[i]
        tr.append($("<td></td>").html(s.name));
        tr.append($("<td></td>").html(s.difficulty));
        tr.append($("<td></td>").html(s.turns));
        tr.append($("<td></td>").html(s.score));
  var d = new Date(s.time);
 tr.append($("<td class='hexed-time'></td>").html(d.format("h:MM:ss TT, m/dd/yy")));
      tbl.append(tr);      
    }
    return tbl;
  }
};


//header for jQuery plugin
(function( $ ) {
  $.fn.hexed = function(settings) {
/////// BEGIN Hexed! \\\\\\\\\\\\\\\\\\

if(!settings.difficulty) settings.difficulty = 5;
if(!settings.turns || settings.turns <=0 ) settings.turns = 10;
var currentTurn = 0;
var currentColor = generateHexColor();

//make the game window
var me = $(this);  //bind the jQuery object of 'this' to me
me.addClass("hexed-window clearfix");
me.append("<h2>Hexed</h2>");

me.append("<h3 id='hexed-total-score'></h3>");

var left = $("<div class='hexed-left'></div>");
me.append(left);

//a turn by turn history
var history = $("<div class='hexed-history'></div>");
history.append("<ul id='hexed-history-list'></ul>");

//the main swatch
var swatch = $("<div class='hexed-swatch'></div>").appendTo(left);
swatch.css('background-color', currentColor.hex());

var countdown = $("<div class='hexed-countdown'></div>").appendTo(left);
var totalScoreDiv = $("<div class='hexed-score'></div>").appendTo(left);
var turnDiv = $("<div class='hexed-turn'>Press to start!</div>").appendTo(left);
var btnDiv = $("<div id='hexed-hexed-btns'></div>").appendTo(left);
var startBtn = $("<button>Start Game!</button>").click(startGame).appendTo(btnDiv);

var redcol = $("<div class='hexed-slider-col'></div>");
var redInput = $("<input type='text' id='hexed-redVal' value='0'/>");

/// Sliders

var redSlider = $("<div id='hexed-redSlide'></div>");
redInput.change(function(){
  redSlider.slider("option", "value", parseInt(redInput.val(), 16));
});
redSlider.slider({
                  handle: '#hexed-slider-handle', 
                  min: 0, 
                  max: 255, 
                  orientation: 'vertical',
                  change: function(evt, ui) {
                    redInput.val(ui.value.toString(16));
                  },
                  slide: function(evt, ui) {
                    redInput.val(ui.value.toString(16));
                  }
                  });
redcol.append(redSlider);
redcol.append(redInput);
redcol.append("<label>Red</label>")
                  
var bluecol = $("<div class='hexed-slider-col'></div>");
var blueInput = $("<input type='text' id='hexed-blueVal' value='0'/>");
var blueSlider = $("<div id='hexed-blueSlide'></div>");
blueInput.change(function(){
  blueSlider.slider("option", "value", parseInt(blueInput.val(), 16));
});
blueSlider.slider({handle: '#hexed-slider-handle', min: 0, max: 255, orientation: 'vertical',
               change: function(evt, ui) {
                 blueInput.val(ui.value.toString(16));
               },
               slide: function(evt, ui) {
                    blueInput.val(ui.value.toString(16));
                  }});
bluecol.append(blueSlider);
bluecol.append(blueInput);
bluecol.append("<label>Blue</label>")
               
var greencol = $("<div class='hexed-slider-col'></div>");
var greenInput = $("<input type='text' id='hexed-greenVal' value='0'/>");
var greenSlider = $("<div id='hexed-greenSlide'></div>");
greenInput.change(function(){
  greenSlider.slider("option", "value", parseInt(greenInput.val(), 16));
});
greenSlider.slider({handle: '#hexed-slider-handle', min: 0, max: 255, orientation: 'vertical',
               change: function(evt, ui) {
                 greenInput.val(ui.value.toString(16));
               },
               slide: function(evt, ui) {
                    greenInput.val(ui.value.toString(16));
                  }});
greencol.append(greenSlider);
greencol.append(greenInput);
greencol.append("<label>Green</label>")
               

/// Put it all together
var sliderDiv = $("<div id='hexed-sliders'></div>");
sliderDiv.append("<h3 id='hexed-slider-label'>Choose Hex Values!</h3>")
sliderDiv.append(redcol);
sliderDiv.append(greencol);
sliderDiv.append(bluecol);
me.append(sliderDiv);

//a function to randomly generate a new color
function generateHexColor() {
  var r = Math.floor(Math.random() * 255);
  var g = Math.floor(Math.random() * 255);
  var b = Math.floor(Math.random() * 255);
  return new Color(r,g,b);
}

//holds the start of the turn
var startTime;

//bind the event handlers
var submitBtn = $("<button>Got It!</button>").click(submit);
var nextBtn   = $("<button>Next</button>").click(doTurn);

//prepare the next turn
function doTurn() {
  if(currentTurn>settings.turns) {
    return;
  }
  nextBtn.attr('disabled','');
  submitBtn.removeAttr('disabled');
  turnDiv.html(currentTurn + " out of "+settings.turns)
  currentColor = generateHexColor();
  swatch.css('background-color', currentColor.hex());
  startTime = new Date();
}

//some more markup
var userColorDiv = $("<div id='userguess' class='hexed-swatch'></div>");
left.append(userColorDiv);

var turnInfo = $("<div class='hexed-turn-info'>&nbsp;</div>");

var turnScore = $("<div class='hexed-turn-score'>&nbsp;</div>").appendTo(turnInfo);

var rscore = $("<div class='hexed-turn-red'>&nbsp;</div>").appendTo(turnInfo);
var gscore = $("<div class='hexed-turn-green'>&nbsp;</div>").appendTo(turnInfo);
var bscore = $("<div class='hexed-turn-blue'>&nbsp;</div>").appendTo(turnInfo);

left.append(turnInfo);

var totalScore;

//handle submission
function submit() {
  nextBtn.removeAttr('disabled');
  submitBtn.attr('disabled','')
  var userColor = new Color(parseInt(redInput.val(),16), parseInt(greenInput.val(),16), parseInt(blueInput.val(),16));
 
  //alert(userColor.hex());
  userColorDiv.css('background-color', userColor.hex());
  var rdiff = Math.abs((userColor.r - currentColor.r)) / 255;
  var gdiff = Math.abs((userColor.g - currentColor.g)) / 255;
  var bdiff = Math.abs((userColor.b - currentColor.b)) / 255;
  var avg = (rdiff+gdiff+bdiff)/3;
  var time = (new Date()).getTime()-startTime.getTime();
  var score = Math.round(((15 - settings.difficulty-avg)/(15-settings.difficulty))*(15000-time));
  if(score < 0 || time > 15000) score = 0;
  rscore.html("Red Score: "+(rdiff*100) + "%");
  gscore.html("Green Score: "+(gdiff*100)+"%");
  bscore.html("Blue Score: "+(bdiff*100)+"%");
  turnScore.html("Turn Score: "+score +' points');
  $("#hexed-history-list", history).append($("<li></li>").append($("<span class='hexed-history-turn'></span>")
                                    .html(currentTurn))
                                  .append($("<div class='hexed-swatch'></div>")
                                    .css('background-color',currentColor.hex()))
                                  .append($("<div class='hexed-swatch'></div>")
                                    .css('background-color', userColor.hex()))
                                  .append($("<span class='hexed-score'></span>")
                                    .html(score) ));
  
  totalScore += score;
  $("#hexed-total-score").html("Total Score: "+totalScore);
  currentTurn++;
  if(currentTurn>settings.turns) {
    endGame();
  }
}

//Ends the game, does high scores and cleanup
function endGame() {
  me.after(history);
  history.append("<h3>Total Score: " + totalScore+ "</h3>");
  //Add the high scores and stuff when we submit the form
  history.delegate("form", 'submit', function() {
    var score = new Score($('#hexed-player-name').val(),
                          settings.difficulty,
                          settings.turns,
                          totalScore,
                          (new Date()).getTime());
    HexedHighScores.addScore(score);
    ("form",history).hide(); //hide the form, we dont need it
  });
  history.append("\
  <form action='sikanp_scores.html'><label>Enter your name</label><input id='hexed-player-name' type='text' />\
  <input type='submit' value='Save Score'></form>\
  ");
  history.css('display','block');  //show the turn by turn history
  btnDiv.html(''); //get rid of these buttons
}

//Starts the hexed game. Called when user hits start button
function startGame() {
  $("#hexed-total-score").html("Total Score: 0");
  currentTurn=1;
  totalScore = 0;
  btnDiv.html(submitBtn);
  btnDiv.append(nextBtn);
  doTurn();
}
///////END Hexed! \\\\\\\\\\\\\\\\\\\\

}; //close $.fn.hexed

//close function( $ )
})( jQuery );



//Package the high score functionality in another plugin
(function( $ ) {
  $.fn.hexedScores = function() {
    this.addClass("hexed-scores");
    var tbl = HexedHighScores.makeTable();
    $(this).append(tbl);
  }; //close $.fn.hexedScores

//close function( $ )
})( jQuery );


