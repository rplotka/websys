/**
 * @file
 * jQuery plugin.
 *
 * Hexed, a light-weight game for measuring a user's accuracy in guessing the
 * correct hex value of a given color.
 *
 */
(function ($, window, document, undefined) {
  // Our plugin's internal methods
  var methods = {
    init: function (settings) {

      // Shortcut to our container selector, set font-size for em conversion
      $this = $(this).css({"font-size": "62.5%", "overflow": "hidden", "min-width": "57em"});

      // Our helper markup that contains the UI for the game
      var c = $("<canvas>", {id: 'hexed-color-block'}).appendTo(this)
        , g = $("<canvas>", {id: 'hexed-guess-block'}).appendTo(this).css({visibility: 'hidden'})
        , s = $("<div>", {id: 'hexed-color-sliders'}).appendTo(this)
        , us = $("<ul>", {id: 'hexed-scores-list', class: 'hexed-scores'}).appendTo(this).before("<h1>High Scores</h1>")
        , uc = $("<ul>", {id: 'hexed-scores-current', class: 'hexed-scores'}).appendTo(this).before('<h1 id="hexed-current-game">Current Game</h1>')

        // Our color sliders
        , sr = $("<div>", {id: 'hexed-slider-red', class: 'hexed-color-slider'}).appendTo(s)
        , sg = $("<div>", {id: 'hexed-slider-green', class: 'hexed-color-slider'}).appendTo(s)
        , sb = $("<div>", {id: 'hexed-slider-blue', class: 'hexed-color-slider'}).appendTo(s)

        // Holds the values that the sliders represent
        , i = $("<i>#</i>").appendTo(s)
        , rv = $("<span>", {id: 'hexed-red-value', class: 'hexed-slider-value'}).html(default_value.toString(16)).appendTo(s)
        , gv = $("<span>", {id: 'hexed-green-value', class: 'hexed-slider-value'}).html(default_value.toString(16)).appendTo(s)
        , bv = $("<span>", {id: 'hexed-blue-value', class: 'hexed-slider-value'}).html(default_value.toString(16)).appendTo(s)
        , b = $("<button>", {type: 'button'}).html("I got this!").appendTo(s)

        // Holds our generated color
        , hex = {r: 0, g: 0, b: 0}

        // If we are generating a new game or not
        , next = false

        // Update the guesses based on the slider's position
        , timer = 0
        , update = function (done) {
          if (timer === 0) {
            timer = new Date();
          }
          var h = {r: sr.slider("value").toString(16), g: sg.slider("value").toString(16), b: sb.slider("value").toString(16)}
            , c = g[0].getContext("2d")
            , t = new Date() - timer
            , a = {};

          // Append leading zero to any value that needs it
          for (var i in h) {
            if (h[i].length !== 2) {
              h[i] = '0' + h[i];
            }
          }
          // Update our values
          rv.html(h.r);
          gv.html(h.g);
          bv.html(h.b);

          // Fill our guess color block
          c.fillStyle = '#' + h.r + h.g + h.b;
          c.fillRect(0, 0, g[0].width, g[0].height);

          // Percent error
          t = t < 0 ? 0 : t;
          h = {r: parseInt(h.r, 16), g: parseInt(h.g, 16), b: parseInt(h.b, 16)};
          a.r = Math.abs(hex.r - h.r) / 255 * 100;
          a.g = Math.abs(hex.g - h.g) / 255 * 100;
          a.b = Math.abs(hex.b - h.b) / 255 * 100;

          if (done === true && turns < settings.turns) {
            score = Math.floor((15 - settings.difficulty - ((a.r + a.g + a.b) / 3)) / (15 - settings.difficulty) * (15000 - t));
            score = score >= 0 ? score : 0;
            turns++;
            methods.appendScore(uc, hex, h, score, t, a);
            total_score += score;
            $("#hexed-current-game").html("Current Game (" + total_score + " pts.)");
            timer = 0;
          }
        }

      // Initialize sliders
      s.find(".hexed-color-slider").slider({
        orientation: "horizontal",
        range: "min",
        max: 255,
        value: default_value,
        slide: update,
        change: update
      });

      // Generate our color that the user has to guess!
      hex = methods.generate(c);

      // Bind to our submit button
      $this.on("click", "#hexed-color-sliders button", function () {
        console.log('here');
        if (next) {
          s.find(".hexed-color-slider").slider("value", default_value);
          hex = methods.generate(c);
          g.css({visibility: 'hidden'});
          b.html("I got this!");
          next = false;
          return true;
        }
        update(true);
        g.css({visibility: 'visible'});

        if (turns >= settings.turns) {
          b.css({background: '#fefefe', color: '#666', border: '2px solid #666'}).html("New Game!");
          $this.off().on("click", "#hexed-color-sliders button", function (e) {
            console.log('no Im here');
            e.stopPropagation();
            $this.off().empty().hexed();
          });

          methods.promptInitials();
          return false;
        }
        else {
          b.html("Next!");
          next = true;
        }
      });
    },

    // Append our score to our list
    appendScore: function (list, hex, guess, score, time, error) {
      var hc = '<span style="background: rgb(' + hex.r + ',' + hex.g + ',' + hex.b + ')"></span>'
        , gc = '<span style="background: rgb(' + guess.r + ',' + guess.g + ',' + guess.b + ')"></span>';
      $(list).append('<li>' + hc + gc + score + 'pts / ' + (time / 1e3).toFixed(2) + 's <small>(R: ' + error.r.toFixed(2) + '%, G: ' + error.g.toFixed(2) + '%, B: ' + error.b.toFixed(2) + '%)</small></li>');
    },

    // Generate our random color block; we use the "canvas" element to hide the
    // RGB values from the user (as best we can).
    generate: function (el) {

      // Generate random hex and fill in our canvas element
      var c = el[0].getContext("2d");
      c.fillStyle = '#' + Math.floor(Math.random() * 16777215).toString(16); /* http://bit.ly/r2CM2 */
      c.fillRect(0, 0, el[0].width, el[0].height);

      return {r: parseInt(c.fillStyle.substr(1,2), 16), g: parseInt(c.fillStyle.substr(3, 2), 16), b: parseInt(c.fillStyle.substr(5, 2), 16)};
    },

    // This method will update HTML5 localStorage (if supported by the browser)
    updateStorage: function (score, initials) {
      var scores = JSON.parse(localStorage.getItem("scores")) || []
        , last = {s: score, i: initials, t: $.datepicker.formatDate("mm-dd-yy", new Date()), d: settings.difficulty, tu: settings.turns};
      scores.push(last);
      localStorage.setItem("scores", JSON.stringify(scores));
    },

    // Load our scores from HTML5 localStorage (if supported by the browser)
    loadStorage: function () {
      var scores = JSON.parse(localStorage.getItem("scores")) || []
        , u = $("#hexed-scores-list").html("");

      // Sort our scores by points
      scores.sort(function (a, b) {
        return a.s === b.s ? 0 : (a.s < b.s ? 1 : -1);
      });

      for (var j in scores) {
        u.append("<li>" + scores[j].s + " pts. by <b>" + scores[j].i + "</b> / " + scores[j].t + "</li>");
      }
    },

    // Prompt user for their initials to save high-score
    promptInitials: function () {
      // Unbind our event for game reinitialization
      $(document).off("keyup", "#hexed-user-prompt input");

      var d = $("<div>", {id: 'hexed-user-prompt'}).appendTo("body")
        , p = $("<div>").appendTo(d).html("<h1>High Score!</h1><h2>" + total_score + " pts.</h2>")
        , i = $("<input>", {type: 'text', name: 'initials[]', placeholder: 'a', maxlength: 1});

      // Append 3 input fields for initials
      i.appendTo(p).focus();
      i.clone().appendTo(p);
      i.clone().appendTo(p);

      $(document).on("keyup", "#hexed-user-prompt input", function (el) {
        var v = $(el.target).val().toUpperCase()
          , c = el.keyCode;

        // If invalid key is entered, we don't move our prompt
        if (v === undefined || c < 65 || c > 90) {
          $(el.target).css({border: '1px solid red'});
          return false;
        }
        $(el.target).css({border: 0});
        initials += v;

        // If we reach the last input, then submit the score
        if ($(el.target).index("input") === 2 && initials.length === 3) {
          methods.updateStorage(total_score, initials);
          methods.loadStorage();
          d.remove();
          return false;
        }
        // Move to the next input
        $(el.target).next("input").focus();
      });
    }
  }
  // Our default value for the slider
  , default_value

  // Current score for the user
  , score

  // Total score for the user
  , total_score

  // Current user's initials
  , initials

  // Holds current number of turns used
  , turns

  // Holds our settings for our game
  , settings;

  // Initialize our plugin
  $.fn.hexed = function (options) {
    // Default settings for our plugin
    settings = $.extend({
      difficulty: 5,
      turns: 10
    }, options);

    // Additional game-specific settings
    default_value = 127;
    score = total_score = turns = 0;
    initials = "";

    // Start our game!
    return this.each(function() {
      methods.init.apply(this, [settings]);

      // Load previous scores from localStorage (if available)
      if (window.localStorage) {
        methods.loadStorage();
      }
    });
  };

})(jQuery, window, window.document);
