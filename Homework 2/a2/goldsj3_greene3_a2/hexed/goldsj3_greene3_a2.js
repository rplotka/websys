jQuery.fn.hexed = function(JSONsettings) /* jQuery Function -> jQuery.hexed(JSON_args) */
{
    var settings = jQuery.parseJSON(JSONsettings);
    if(settings.difficulty == null)
    {
		settings.difficulty = 5;
    }
    if(settings.turns == null)
    {
		settings.turns = 10;
    }
    if(settings.mode == "god")
    {
		window.god = 1;
    }
    else
    {
		window.god = 0;
    }

	/* Global variables */
    window.difficulty = settings.difficulty;
    window.total_turns = settings.turns;
    window.hexed = this;
    initialize();
    /* Global variables */
};

function initialize() /* All initialization functions to start a new game. Can be used to restart the game as well. */
{
	/* Global variables */
    window.turns_rem = window.total_turns;
    window.rgb = new Array();
    window.answer = new Array();
    window.points = 0;
    /* Global variables */
    
    jQuery(window.hexed).html("");
    setup_actual(window.hexed);
    setup_guess(window.hexed);
    generate_random_color();
    update_preview();
    window.ms = get_milliseconds();
}

function setup_actual(doc) /* Creates area with the color the player has to guess */
{
    jQuery(doc).append('<div id="actual" class="panel"></div>');
    jQuery("#actual").append('<div id="real" class="view_window"></div>');
    jQuery("#real").append('<div id="guess_preview"></div>');
}

/* creates area for guessing, sets up a preview of the color you've created(only active in god mode),
 sets up the sliders and sets up button used for guessing */
function setup_guess(doc) 
{
    jQuery(doc).append('<div id="guess" class="panel"></div>');
    
    jQuery("#guess").append('<div id="preview" class="view_window"></div>');
    if(window.god != 1)
		jQuery("#preview").css("display","none");
	
	setup_slider("red");
    setup_slider("green");
    setup_slider("blue");
    
    setup_button();
}

function setup_slider(color) /* creates a slider area and then initializes the slider */
{
    window.rgb[color] = 0;
    jQuery("#guess").append('<div class="slider_holder" id="slider_holder_'+color+'"></div>');
    jQuery("#slider_holder_"+color).append('<div class="slider" id="slider_'+color+'"></div>');
    jQuery("#slider_"+color).slider(
	{
            orientation: "vertical",
            range: "min",
            min: 0,
            max: 255,
            value: 0,
            slide: function( event, ui )
            {
				window.rgb[color] = ui.value;
				jQuery("#hex_box_"+color).val(ui.value.toString(16));
				update_preview();
            }
        }
    );
    jQuery("#slider_holder_"+color).append('<input maxlength="2" class="hex_box" id="hex_box_'+color+'" type="text" size="2" />');
    jQuery("#hex_box_"+color).val(window.rgb[color].toString(16));

    /* Setup the input box for each slider */
    jQuery("#hex_box_"+color).change(function()
    {
	
		/* Check to see if valid HEX input */
		if(parseInt(jQuery("#hex_box_"+color).val(),16).toString(16) == jQuery("#hex_box_"+color).val())
		{
		    update_slider(color);
		}
		else /* Revert input box value */
		{
		    jQuery("#hex_box_"+color).val(window.rgb[color].toString(16));
		}
    });
}

function update_slider(color) /* Makes the slider reflect the value in the input box */
{
    window.rgb[color] = parseInt(jQuery("#hex_box_"+color).val(),16);
    jQuery("#slider_"+color).slider({value: window.rgb[color]});
    update_preview();
}

function setup_button() /* Sets up the Got it button, which is also reused as a Next button */
{
    jQuery("#guess").append('<div id="button_holder"></div>');
    jQuery("#button_holder").append('<input id="button" type="button" value="Got It!" />');
    jQuery("#button").click(function() { button_click(); });
}

function update_preview() /* Used to update player's preview in god mode */
{
    jQuery("#preview").css("background-color","rgb("+window.rgb["red"]+","+window.rgb["green"]+","+window.rgb["blue"]+")");
}

function generate_random_color() /* Used to generate the random color for the player to guess */
{
    window.answer["red"]   = Math.floor(Math.random()*255);
    window.answer["green"] = Math.floor(Math.random()*255);
    window.answer["blue"]  = Math.floor(Math.random()*255);
    jQuery("#real").css("background-color","rgb("+window.answer["red"]+","+window.answer["green"]+","+window.answer["blue"]+")");
}

function get_milliseconds() /* Returns a unix timestamp */
{
    var d = new Date();
    return d.getTime();
}

function submit_score() /* Check to make sure user inputted valid input before actually submitting their score */
{
    if(jQuery("#name").val() == "")
    {
		alert("Please enter a name.");
    }
    else
    {
		add_score(jQuery("#name").val(),Math.round(window.points));
		jQuery("#name").after(jQuery("#name").val()).remove();
		jQuery("#submit_score").remove();
    }
}

/* adds a players score to the scores JSON string by parsing and strinifying the entire list and appending the new element */
function add_score(player_name,score) 
{
    var timestamp = get_milliseconds();
    var difficulty = window.difficulty;
    var turns = window.total_turns;


    if(typeof(Storage)!=="undefined")
    {
        if(localStorage.scores == undefined) /* Need to add first score */
        {
            localStorage.scores = '{"records": [';
            localStorage.scores += '{';
            localStorage.scores += '"player": "'+player_name+'",';
            localStorage.scores += '"score": "'+score+'",';
            localStorage.scores += '"timestamp": "'+timestamp+'",';
            localStorage.scores += '"difficulty": "'+difficulty+'",';
            localStorage.scores += '"turns": "'+turns+'"';
            localStorage.scores += '}';
            localStorage.scores += ']}';
        }
        else /* add subsequent score */
        {
            var scores = jQuery.parseJSON(localStorage.scores);
            scores = scores.records;
            localStorage.scores = '{"records": [';
            var added = 0;
            for(var i = 0;i<scores.length; i++)
            {
                if(!added && score > scores[i].score) /* Score hasn't been added yet. */
                {
                    localStorage.scores += '{';
                    localStorage.scores += '"player": "'+player_name+'",';
                    localStorage.scores += '"score": "'+score+'",';
                    localStorage.scores += '"timestamp": "'+timestamp+'",';
                    localStorage.scores += '"difficulty": "'+difficulty+'",';
                    localStorage.scores += '"turns": "'+turns+'"';
                    localStorage.scores += '},';
                    added = 1;
                }
                localStorage.scores += '{';
                localStorage.scores += '"player": "'+scores[i].player+'",';
                localStorage.scores += '"score": "'+scores[i].score+'",';
                localStorage.scores+= '"timestamp": "'+scores[i].timestamp+'",';
                localStorage.scores+= '"difficulty": "'+scores[i].difficulty+'",';
                localStorage.scores+= '"turns": "'+scores[i].turns+'"';
                localStorage.scores += '}';
		if(i < scores.length-1 || !added)
		{
		    localStorage.scores += ",";
		}
            }

            if(!added) /* Score hasn't been added yet. */
            {
                localStorage.scores += '{';
                localStorage.scores += '"player": "'+player_name+'",';
                localStorage.scores += '"score": "'+score+'",';
                localStorage.scores += '"timestamp": "'+timestamp+'",';
                localStorage.scores += '"difficulty": "'+difficulty+'",';
                localStorage.scores += '"turns": "'+turns+'"';
                localStorage.scores += '}';
            }
            localStorage.scores += ']}';
            console.log(localStorage.scores);
        }
    }
    else
    {
        console.log("No web storage available. :(");
    }
}

function load_scoreboard()
{
    if(typeof(Storage)!=="undefined")
    {
		if(localStorage.scores == undefined)
		{
		    jQuery(".scoreboard").append("<tr><td>No scores submitted</td></tr>");
		}
	else
	{
	    var scores = jQuery.parseJSON(localStorage.scores);
	    scores = scores.records;
	    jQuery(".scoreboard").append('<tr><td class="player">Player Name</td><td class="score">Score</td><td class="difficulty">Difficulty</td><td class="turns">Rounds Played</td><td class="date">Timestamp</td></tr>');
	    for(var i = 0; i < scores.length; i++) // iterate through all scores
	    {
		var date = unix_to_date(scores[i].timestamp*1);
		jQuery(".scoreboard").append('<tr><td class="player">'+scores[i].player+'</td><td class="score">'+scores[i].score+'</td><td class="difficulty">'+scores[i].difficulty+'</td><td class="turns">'+scores[i].turns+'</td><td class="date">'+date+'</td></tr>');
	    }
	}
    }
    else
    {
		jQuery(".scoreboard").append("<tr><td>Local storage not supported</td></tr>");
    }
}

function unix_to_date(time)
{
    var date = new Date(time);
    var hours = +date.getHours();
    var timeofday = "AM";
    if(hours > 12)
    {
	hours = hours % 12;
	timeofday = "PM";
    }
    if(hours < 10)
    {
	hours = "0"+hours;
    }
    var minutes = date.getMinutes();
    if(minutes < 10)
    {
	minutes = "0"+minutes;
    }
    var seconds = date.getSeconds();
    if(seconds < 10)
    {
        seconds = "0"+seconds;
    }


    return date.toDateString()+" "+hours+":"+minutes+":"+seconds+" "+timeofday;
}





function button_click()
{
    var mode = jQuery("#button").val();
    if(mode == "Got It!")
    {
	jQuery("#guess_preview").css("background-color","rgb("+window.rgb["red"]+","+window.rgb["green"]+","+window.rgb["blue"]+")");
	/* Calculate averages */
	var r_err = Math.round((window.rgb["red"]-window.answer["red"])*100/255);
	var g_err = Math.round((window.rgb["green"]-window.answer["green"])*100/255);
	var b_err = Math.round((window.rgb["blue"]-window.answer["blue"])*100/255);
	var avg_err = Math.round((Math.abs(r_err)+Math.abs(g_err)+Math.abs(b_err))/3);
	
	/* Calculate score */
	var score = ((15 - window.difficulty - avg_err) / (15 - window.difficulty)) * (15000 - (get_milliseconds()-window.ms));
	if(score < 0)
	    score = 0;

	window.points += score;

	jQuery("#guess_preview").html("Red: "+r_err+"%<br/>Green: "+g_err+"%<br/>Blue: "+b_err+"%<br/>Avg: "+avg_err+"%").css("color","rgb("+(255-window.rgb["red"])+","+(255-window.rgb["green"])+","+(255-window.rgb["blue"])+")");
	jQuery("#button").val("Next");
	jQuery("#guess_preview").css("visibility","visible");
    }
    else if(mode == "Next")
    {
	if(--(window.turns_rem) > 0) /* More turns left */
	{
	    jQuery("#guess_preview").css("visibility","hidden");
	    jQuery("#button").val("Got It!");
	    generate_random_color();
	    window.ms = get_milliseconds();
	}
	else /* Game ended. */
	{
	    jQuery(window.hexed).html('Final Score: '+Math.round(window.points)+'<br/>');

	    if(typeof(Storage)!=="undefined") /* Only allow score submission if local storage is supported */
	    {
		jQuery(window.hexed).append('Name: <input type="text" id="name" /> <br/> <input type="button" id="submit_score" value="Submit Score" />');
		jQuery("#submit_score").click(function() { submit_score(); });
	    }

	    jQuery(window.hexed).append('<input type="button" id="play_again" value="Play Again?" />');
	    jQuery("#play_again").click(function() { initialize(); });
	}
    }
}

function play_again()
{
    jQuery(window.hexed).html( window.pagesave );
    jQuery("#guess_preview").css("visibility","hidden");
    jQuery("#button").val("Got It!");
    generate_random_color();
    window.ms = get_milliseconds();
}

