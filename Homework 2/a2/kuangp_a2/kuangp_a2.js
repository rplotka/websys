// Moves slider depending on hex value set on inputs
function setcolor(id) {
    var x = document.getElementById(id);
    if( x.value.length < 3 ) {
        // Converts hex to decimal value
        var color = parseInt(x.value, 16);
        // Validates input
        if( color >= 0 || color <= 255 ) {
            if( id == "red" ){ $( "#slider1" ).slider("value", color ); }
            if( id == "green" ){ $( "#slider2" ).slider("value", color ); }
            if( id == "blue" ){ $( "#slider3" ).slider("value", color ); }
        }
    }
}
// Converts decimal value to Hex 
function toHex( value ) {
    var x = value.toString( 16 ).toUpperCase();
    if( x.length === 1 ) { x = "0" + x; }
    return x;
}
// Generates random values for RGB and set swatch color
function setRandomColor() {
    var r = Math.floor(Math.random()*256);
    var g = Math.floor(Math.random()*256);
    var b = Math.floor(Math.random()*256);
    r = toHex( r );
    g = toHex( g );
    b = toHex( b );
    var color = "#" + r + g + b;
    $( "#swatch" ).css( {'background-color' : color} );
}
// Function for resetting game values
function restart() {
    // Resets initial values
    document.getElementById( "score" ).innerHTML = "Score: 0";
    document.getElementById( "rederror" ).innerHTML = "0%";
    document.getElementById( "greenerror" ).innerHTML = "0%";
    document.getElementById( "blueerror" ).innerHTML = "0%";
    $( "#resultColor" ).css( {"background": "#FFFFFF"} );
    setRandomColor();
    $( "#slider1, #slider2, #slider3" ).slider( "option", "value", 0 );
    $( "#red" ).val( $( "#slider1" ).slider( "value" ) + "0" );
    $( "#green" ).val( $( "#slider2" ).slider( "value" ) + "0" );
    $( "#blue" ).val( $( "#slider3" ).slider( "value" ) + "0" );
    var newstart = new Date().getTime();
    $( "#title" ).data( "start", newstart );
}
// CallBack function to start next turn
function next() {
    // Calculate running total
    var total = $( "#totalscore" ).data( "totalscore" ) + $( "#score" ).data( "score" );
    document.getElementById( "totalscore" ).innerHTML = "Total Score: " + total;
    $( "#totalscore" ).data( "totalscore", total );
    restart();
    // Increment turns counter
    var turn = $( "#turn" ).data( "turn" ) + 1;
    document.getElementById( "turn" ).innerHTML = "Turn: " + turn;
    $( "#turn" ).data( "turn", turn );
    // Remove the next button
    $( "#next" ).remove();
}
// CallBack function to restart game
function replay() {
    restart();
    // Reset all stored values
    $( "#turn" ).data( "turn", 1 );
    $( "#totalscore" ).data( "totalscore", 0 );
    $( "#score" ).data( "score", 0 );
    document.getElementById( "totalscore" ).innerHTML = "Total Score: 0";
    document.getElementById( "turn" ).innerHTML = "Turn: 1";
    // Remove buttons and score input elements
    $( "#replay" ).remove();
    $( "#gameover" ).remove();
    $( "#entername" ).remove();
    $( "#submitscore" ).remove();
    $( "#highscore" ).remove();
}
// CallBack function for score submission
function highscores() {
    var name = document.getElementById( "highscore" ).value;
    // Validates user input
    if( name.length != 0 ) {
        // Gather all game information
        var score = $( "#totalscore" ).data( "totalscore" );
        var difficulty = $( "#level" ).data( "diff" );
        var turns = $( "#button" ).data( "numturns" );
        var date = new Date();
        // Get timestamp information from Date object
        // Reference: http://www.tizag.com/javascriptT/javascriptdate.php
        var timestamp = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + " " + date.getHours();
        if( date.getMinutes() < 10 ) { timestamp = timestamp + ":0" + date.getMinutes(); }
        else { timestamp = timestamp + ":" + date.getMinutes(); }
        if( date.getSeconds() < 10 ) { timestamp = timestamp + ":0" + date.getSeconds(); }
        else { timestamp = timestamp + ":" + date.getSeconds(); }
        // Checks  for localStorage capability
        // Reference: http://fuzzytolerance.info/using-localstorage/
        if( window.localStorage ) {
            // Checks if "highscores" data has been initialized
            if( localStorage.getItem( "highscores" ) ) {
                // Parse JSON string
                var highscore = eval( "(" + localStorage.getItem( "highscores" ) + ")" );
                var newhighscore = { "name": name, "score": score, "difficulty": difficulty, "turns": turns, "timestamp": timestamp };
                // Add new JSON object to JSON array 
                highscore.push( newhighscore );
                // Stringify JSON object for localstorage
                // Reference: http://www.json.org/js.html
                highscore = JSON.stringify( highscore );
                localStorage.setItem( "highscores" , highscore );
            }
            else {
                // If "highscores" not initialized, create the JSON string and
                // store in localstorage
                var newhighscore = '[{ "name":"' + name + '", "score":' + score + ', "difficulty":"' + difficulty + '", "turns":"' + turns + '", "timestamp":"' + timestamp + '" }]';
                localStorage.setItem( "highscores" , newhighscore );
            }
        }
    }
    // Removes buttons and prompt elements
    $( "#entername" ).remove();
    $( "#submitscore" ).remove();
    $( "#highscore" ).remove();
}
// Function to be called at end of game
function endgame() {
    // Calculate running total score
    var total = $( "#totalscore" ).data( "totalscore" ) + $( "#score" ).data( "score" );
    document.getElementById( "totalscore" ).innerHTML = "Total Score: " + total;
    $( "#totalscore" ).data( "totalscore", total );
    // Add end of game elements ( prompts, name input, submit score and play
    // again buttons )
    $( "#result" ).append('<div id="gameover">Game Over!</div><div id="entername">Name:&nbsp;</div><input id="highscore" type="text" /><button id="submitscore" type="button" onclick="highscores()">Submit Score</button><button id="replay" type="button" onclick="replay()">Play Again?</button>');
}
// CallBack function for when user hits submit
function submit() {
    // Checks if submit has not been hit for current turn yet
    if( document.getElementById( "next" ) == null ) {
        // Gather scoring information
        var start = parseInt($( "#title" ).data( "start" ));
        var difficulty = parseInt($( "#level" ).data( "diff" ));
        var elapsed = parseInt( new Date().getTime() - start );
        // Read user inputs
        var r = document.getElementById( "red" ).value;
        var g = document.getElementById( "green" ).value;
        var b = document.getElementById( "blue" ).value;
        // Input Error handling
        if( r.length > 2 || g.length > 2 || b.length > 2 || parseInt( r, 16 ) < 0|| parseInt( g, 16 ) < 0|| parseInt( b, 16 ) < 0 || isNaN(parseInt( r, 16 )) || isNaN(parseInt( g, 16 )) || isNaN(parseInt( b, 16 )) ) {
            document.getElementById( "error" ).innerHTML = "ERROR: Invalid Input. Try again.";
            return;
        }
        // Removes error prompt if valid input
        document.getElementById( "error" ).innerHTML = "";
        var color = "#" + r + g + b;
        // Shows user submitted color
        $( "#resultColor" ).css( {"background": color} );
        // Reads swatch color 
        var swatch = $( "#swatch" ).css( "background-color" );
        // If script returns a Hex value
        if( swatch.charAt(0) == "#" ) {
            var rError = swatch.charAt(1) + swatch.charAt(2);
            var gError = swatch.charAt(3) + swatch.charAt(4);
            var bError = swatch.charAt(5) + swatch.charAt(6);
            // Convert to decimal values
            rError = parseInt( rError, 16 );
            gError = parseInt( gError, 16 );
            bError = parseInt( bError, 16 );
        }
        else { //If script returns a rgb() value
            // User RegEx to parse value 
            // Reference: http://haacked.com/archive/2009/12/29/convert-rgb-to-hex.aspx
            var matches = swatch.match(/rgb\((\d+), (\d+), (\d+)\)/);
            var rError = matches[1];
            var gError = matches[2];
            var bError = matches[3];
        }
        // Convert user input to decimal values
        r = parseInt( r, 16 );
        g = parseInt( g, 16 );
        b = parseInt( b, 16 );
        // Calculate error percentages
        rError = Math.round(((r - rError) / 255) * 100);
        gError = Math.round(((g - gError) / 255) * 100);
        bError = Math.round(((b - bError) / 255) * 100);
        // Print errors
        document.getElementById( "rederror" ).innerHTML = rError + "%";
        document.getElementById( "greenerror" ).innerHTML = gError + "%";
        document.getElementById( "blueerror" ).innerHTML = bError + "%";
        // Calculate score
        var avgerror = Math.abs(rError + gError + bError) / 3;
        var score = Math.round(((15 - difficulty - avgerror) / (15 - difficulty)) * (15000 - elapsed));
        if( score < 0 ) { score = 0; }
        document.getElementById( "score" ).innerHTML = "Score: " + score;
        $( "#score" ).data( "score", score );
        // If at final turn, end game else append next button
        if( $( "#turn" ).data( "turn" ) == $( "#button" ).data( "numturns" ) ) { endgame(); }
        else { $( "#result" ).append('<button id="next" type="button" onclick="next()">Next</button>'); }
    }
}
// Main function containing hexed plugin
// Reference: http://docs.jquery.com/Plugins/Authoring
( function( $ ){
    $.fn.hexed = function( options ){
        // Append game to element being called on 
        return this.each(function() {
            $(this).append('<div id="hexed"><h1 id="title">HEXED!</h1><div id="info"><div id="level">Difficulty: 5</div><div id="turn">Turn: 1</div><div id="totalscore">Total Score: 0</div></div><div id="swatch"></div><div id="sliders"><div id="slider1"></div><div id="slider2"></div><div id="slider3"></div></div><div id="inputs"><input type="text" id="red" onkeyup="setcolor(id)" /><input type="text" id="green" onkeyup="setcolor(id)" /><input type="text" id="blue" onkeyup="setcolor(id)" /></div><button id="button" type="button" onclick="submit()">Got It!</button><div id="error"></div><div id="result">Your Color<div id="resultColor"></div><div>% Error</div><div id="rederror">0%</div><div id="greenerror">0%</div><div id="blueerror">0%</div><div id="score">Score: 0</div></div></div>');
            // Apply settings to default if given
            if( options.difficulty <= 10 && options.difficulty >= 0 && options.turns > 0 ) {
                var settings = $.extend( {
                    'difficulty' : '5',
                    'turns' : '10'
                }, options );
            }
            else {
                 var settings = {
                    'difficulty' : '5',
                    'turns' : '10'
                };
            }
            // Get current time information
            // Reference: http://www.electrictoolbox.com/get-milliseconds-javascript-measure-time/
            var start = new Date().getTime();
            // Store state data into html elements 
            // Reference: http://api.jquery.com/data/
            $( "#level" ).data( "diff", settings.difficulty );
            $( "#button" ).data( "numturns", settings.turns );
            document.getElementById( "level" ).innerHTML = "Difficulty: " + settings.difficulty;
            $( "#title" ).data( "start", start );
            $( "#turn" ).data( "turn", 1 );
            $( "#totalscore" ).data( "totalscore", 0 );
            // Initiate jquery slider ui 
            // Reference: http://jqueryui.com/demos/slider/#default
            // Reference: http://docs.jquery.com/UI/API/1.8/Slider#overview
            $( "#slider1, #slider2, #slider3" ).slider({
                orientation: "vertical",
                animate: true,
                range: "min",
                min: 0,
                max: 255,
                value: 00,
            });
            // Set callback functions when user slides sliders
            $( "#slider1" ).slider({
                slide: function( event, ui ) {
                           var color = toHex( ui.value );
                           $( "#red" ).val( color );
                       }
            });
            $( "#slider2" ).slider({
                slide: function( event, ui ) {
                           var color = toHex( ui.value );
                           $( "#green" ).val( color );
                       }
            });
            $( "#slider3" ).slider({
                slide: function( event, ui ) {
                           var color = toHex( ui.value );
                           $( "#blue" ).val( color );
                       }
            });
            // Set initial values in input fields
            $( "#red" ).val( $( "#slider1" ).slider( "value" ) + "0" );
            $( "#green" ).val( $( "#slider2" ).slider( "value" ) + "0" );
            $( "#blue" ).val( $( "#slider3" ).slider( "value" ) + "0" );
            // Initialize swatch to a random color
            setRandomColor();
        });
    };
} )( jQuery );

