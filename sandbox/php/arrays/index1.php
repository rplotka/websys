<?php 

// $months = array('january', 'february', 'march', 'april', 'may');

// alternative syntax for creating arrays in php 5.4+
// $months = ['january', 'february', 'march', 'april', 'may'];

// associative array

$songs = array(
   'Rush' => 'Fly by Night',
   'Genesis' => 'abacab',
   'ELP' => 'Fanfare',
   );




// echo '<pre>';
// var_dump($months);
// print_r($months);
// echo '</pre>';




 ?>

<!doctype html>
<html>
<head>
   <title>My Arrays</title>
</head>

<body>
   <h1>Arrays</h1>
<ul>
   <?php 
   // foreach($months as $month) {
   //    echo "<li>$month</li>";
   // }

   // foreach ($songs as $group => $song) {
   //    echo "<li>$group - " . ucwords($song) . "</li>";
   // }

// alternate syntax - note 5.4 shorthand for echo <?=
   foreach ($songs as $group => $song) : ?>
      <li>
         <?= $group; ?>  -  <?= ucwords($song); ?> 
      </li>
   <?php endforeach ?>



</ul>

<?php




 ?>
</body>
</html>
