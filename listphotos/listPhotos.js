$.ajax({
  url: "pics",
  success: function(data){        
    var i=1; 
    var litem = '<ul class="photo">';

     $(data).find("a:contains(.jpeg)").each(function(){
        // will loop through
        var imagename = $(this).attr("href");

          // create a list item with the photo and the name
        litem += '<li class="pic"><img src="pics/' + imagename + '"/>' + i + ')' + imagename + '</li>'

          // 4 students per line
        if (i%4 == 0) {
          litem += '</ul>';
          $("#photos").append(litem);
          litem = '<ul class="photo">';
        } 
        i++;
     });

         // output remainder if the total is not a multiple of 4
     if (i%4 != 0) {
      litem += '</ul>';
      $("#photos").append(litem);
     }
  }
});