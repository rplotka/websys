      // We'll make this request in the onclick event handler for #trigger.
      $('#trigger').click(function(e) {
        // Make a GET request to the given URL.
        // In the callback, we'll manipulate the response. This callback accepts
        // three arguments: the data, the status, and a jQuery-enhanced
        // XMLHttpRequest object. For now, we just need the data.
        $.get('../snippet.txt', function(data) {
          // In this example, we're hiding the element first, and then replacing
          // the contents of #change-me only after the content is hidden, at
          // which point it will be shown again. This is another example of
          // using callbacks to ensure that functions are executed one after
          // the other, as opposed to immediately.
          $('#change-me').hide(600, function () { 
            // The keyword "this" refers to the current element, #change-me.
            // Below, we're replacing the HTML within #change-me with the data
            // being passed in through our AJAX callback, and then showing it.
            $(this).html(data).show(600); 
          });  
        });
      });