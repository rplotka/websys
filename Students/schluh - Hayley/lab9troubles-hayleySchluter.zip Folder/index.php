<?php 

	require 'config.php';

	try {
		$conn = new PDO('mysql:host=localhost', $config['DB_USERNAME'], $config['DB_PASSWORD']);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	} catch(PDOException $e) {
		echo 'ERROR: ' . $e->getMessage();

	}

?>

<!DOCTYPE HTML>
<html>
<body>
	<form action="install.php" method="get">
		<input type="submit" value="Install">
	</form>
	<form action="insert.php" method="post">
		<h4>Insert Course</h4>
		CRN: <input type="text" name="crn[]">
		Prefix: <input type="text" name="prefix[]">
		Number: <input type="text" name="num[]">
		Title: <input type="text" name="title[]">
		Section: <input type="text" name="section[]">
		Year: <input type="text" name="year[]">
		<input type="submit" name="course" value="Load">
		<h4>Insert Students</h4>
		<h5>Student #1</h5>
		RIN: <input type="text" name="rin[]">
		First Name: <input type="text" name="fname[]">
		Last Name: <input type="text" name="lname[]">
		Address 1: <input type="text" name="address1[]">
		Address 2: <input type="text" name="address2[]">
		<br>
		City: <input type="text" name="city[]">
		State: <input type="text" name="state[]">
		Zip: <input type="text" name="zip[]">
		Zip+4: <input type="text" name="zip4[]">
		<br>
		<h5>Student #2</h5>
		RIN: <input type="text" name="rin[]">
		First Name: <input type="text" name="fname[]">
		Last Name: <input type="text" name="lname[]">
		Address 1: <input type="text" name="address1[]">
		Address 2: <input type="text" name="address2[]">
		<br>
		City: <input type="text" name="city[]">
		State: <input type="text" name="state[]">
		Zip: <input type="text" name="zip[]">
		Zip+4: <input type="text" name="zip4[]">
		<br>
		<h5>Student #3</h5>
		RIN: <input type="text" name="rin[]">
		First Name: <input type="text" name="fname[]">
		Last Name: <input type="text" name="lname[]">
		Address 1: <input type="text" name="address1[]">
		Address 2: <input type="text" name="address2[]">
		<br>
		City: <input type="text" name="city[]">
		State: <input type="text" name="state[]">
		Zip: <input type="text" name="zip[]">
		Zip+4: <input type="text" name="zip4[]">
		<br>
		<h5>Student #4</h5>
		RIN: <input type="text" name="rin[]">
		First Name: <input type="text" name="fname[]">
		Last Name: <input type="text" name="lname[]">
		Address 1: <input type="text" name="address1[]">
		Address 2: <input type="text" name="address2[]">
		<br>
		City: <input type="text" name="city[]">
		State: <input type="text" name="state[]">
		Zip: <input type="text" name="zip[]">
		Zip+4: <input type="text" name="zip4[]">
		<br>
		<h5>Student #5</h5>
		RIN: <input type="text" name="rin[]">
		First Name: <input type="text" name="fname[]">
		Last Name: <input type="text" name="lname[]">
		Address 1: <input type="text" name="address1[]">
		Address 2: <input type="text" name="address2[]">
		<br>
		City: <input type="text" name="city[]">
		State: <input type="text" name="state[]">
		Zip: <input type="text" name="zip[]">
		Zip+4: <input type="text" name="zip4[]">
		<br>
		<h5>Student #6</h5>
		RIN: <input type="text" name="rin[]">
		First Name: <input type="text" name="fname[]">
		Last Name: <input type="text" name="lname[]">
		Address 1: <input type="text" name="address1[]">
		Address 2: <input type="text" name="address2[]">
		<br>
		City: <input type="text" name="city[]">
		State: <input type="text" name="state[]">
		Zip: <input type="text" name="zip[]">
		Zip+4: <input type="text" name="zip4[]">
		<br>
		<h5>Student #7</h5>
		RIN: <input type="text" name="rin[]">
		First Name: <input type="text" name="fname[]">
		Last Name: <input type="text" name="lname[]">
		Address 1: <input type="text" name="address1[]">
		Address 2: <input type="text" name="address2[]">
		<br>
		City: <input type="text" name="city[]">
		State: <input type="text" name="state[]">
		Zip: <input type="text" name="zip[]">
		Zip+4: <input type="text" name="zip4[]">
		<br>
		<h5>Student #8</h5>
		RIN: <input type="text" name="rin[]">
		First Name: <input type="text" name="fname[]">
		Last Name: <input type="text" name="lname[]">
		Address 1: <input type="text" name="address1[]">
		Address 2: <input type="text" name="address2[]">
		<br>
		City: <input type="text" name="city[]">
		State: <input type="text" name="state[]">
		Zip: <input type="text" name="zip[]">
		Zip+4: <input type="text" name="zip4[]">
		<br>
		<h5>Student #9</h5>
		RIN: <input type="text" name="rin[]">
		First Name: <input type="text" name="fname[]">
		Last Name: <input type="text" name="lname[]">
		Address 1: <input type="text" name="address1[]">
		Address 2: <input type="text" name="address2[]">
		<br>
		City: <input type="text" name="city[]">
		State: <input type="text" name="state[]">
		Zip: <input type="text" name="zip[]">
		Zip+4: <input type="text" name="zip4[]">
		<br>
		<h5>Student #10</h5>
		RIN: <input type="text" name="rin[]">
		First Name: <input type="text" name="fname[]">
		Last Name: <input type="text" name="lname[]">
		Address 1: <input type="text" name="address1[]">
		Address 2: <input type="text" name="address2[]">
		<br>
		City: <input type="text" name="city[]">
		State: <input type="text" name="state[]">
		Zip: <input type="text" name="zip[]">
		Zip+4: <input type="text" name="zip4[]">
		<br>
		<input type="submit" name="students" value="Load">
		<br><br>
		<h4>Add Grades</h4>
		RIN: <input type="text" name="rin[]">
		CRN: <input type="text" name= "crn[]">
		Grade: <input type="text" name="grade[]">
		<br>
		RIN: <input type="text" name="rin[]">
		CRN: <input type="text" name= "crn[]">
		Grade: <input type="text" name="grade[]">
		<br>
		RIN: <input type="text" name="rin[]">
		CRN: <input type="text" name= "crn[]">
		Grade: <input type="text" name="grade[]">
		<br>
		RIN: <input type="text" name="rin[]">
		CRN: <input type="text" name= "crn[]">
		Grade: <input type="text" name="grade[]">
		<br>
		RIN: <input type="text" name="rin[]">
		CRN: <input type="text" name= "crn[]">
		Grade: <input type="text" name="grade[]">
		<br>
		RIN: <input type="text" name="rin[]">
		CRN: <input type="text" name= "crn[]">
		Grade: <input type="text" name="grade[]">
		<br>
		RIN: <input type="text" name="rin[]">
		CRN: <input type="text" name= "crn[]">
		Grade: <input type="text" name="grade[]">
		<br>
		RIN: <input type="text" name="rin[]">
		CRN: <input type="text" name= "crn[]">
		Grade: <input type="text" name="grade[]">
		<br>
		RIN: <input type="text" name="rin[]">
		CRN: <input type="text" name= "crn[]">
		Grade: <input type="text" name="grade[]">
		<br>
		RIN: <input type="text" name="rin[]">
		CRN: <input type="text" name= "crn[]">
		Grade: <input type="text" name="grade[]">
		<br>
		<input type="submit" name="grades" value="Load">
	</form>
	<form action="output.php" method="post">
		<input type="submit" name="students" value="Students">
		<br>
		<input type="Submit" name="gradeDist" value="Grade Distribution">
	</form>
</body>
</html>