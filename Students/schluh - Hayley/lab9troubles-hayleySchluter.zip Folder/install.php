<?php

	require 'config.php';

	try {
		$conn = new PDO('mysql:host=localhost', $config['DB_USERNAME'], $config['DB_PASSWORD']);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		echo 'Server Connected.'.'</br>';

		$pstmt = $conn->prepare('CREATE DATABASE IF NOT EXISTS websyslab9hayley');
		$status = $pstmt->execute();
		//$conn = new PDO('mysql:host=localhost', $config['DB_USERNAME'], $config['DB_PASSWORD']);
		echo 'Create done. Status='.$status.'</br>';

		$sql = "USE websyslab9hayley";
		$status = $conn->exec($sql);
		echo 'Using. Status = '.$status.'</br>';

		$sql = "CREATE TABLE IF NOT EXISTS students (
			rin int(9) NOT NULL,
			fname varchar(100) NOT NULL,
			lname varchar(100) NOT NULL,
			address1 varchar(100),
			address2 varchar(100),
			city varchar(20),
			state varchar(2),
			zip int(5),
			zip4 int(9),
			PRIMARY KEY(rin)
		) ENGINE=InnoDB DEFAULT CHARSET =UTF8 AUTO_INCREMENT=1;";
		$sql = trim($sql);

		$status = $conn->exec($sql);
		echo 'Create students table. Status = '.$status.'</br>';

		$sql = "CREATE TABLE IF NOT EXISTS courses (
		crn int(11),
		prefix varchar(4) NOT NULL,
		num smallint(4) NOT NULL,
		title varchar(255) NOT NULL,
		section varchar(255),
		year year(4),
		PRIMARY KEY(crn)
		) ENGINE=InnoDB DEFAULT CHARSET=UTF8 AUTO_INCREMENT=1;";
		$sql = trim($sql);

		$status = $conn->exec($sql);
		echo 'Create crouses table. Status = '.$status.'</br>';

		$sql = "CREATE TABLE IF NOT EXISTS grades (
		id int(11) AUTO_INCREMENT,
		rin int(9) NOT NULL,
		crn int(11),
		grade int(3) NOT NULL,
		PRIMARY KEY(id),
		FOREIGN KEY (crn) REFERENCES courses(crn),
		FOREIGN KEY (rin) REFERENCES students(rin)
		) ENGINE=InnoDB DEFAULT CHARSET=UTF8 AUTO_INCREMENT=1;";
		$sql = trim($sql);

		$status = $conn->exec($sql);
		echo 'Create grades table. Status = '.$status.'</br>';


	} catch(PDOException $e) {
		echo 'ERROR: ' . $e->getMessage();

	}

?>
