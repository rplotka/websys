<?php
	require 'config.php';

	try {
		$conn = new PDO('mysql:host=localhost;dbname=websyslab9hayley', $config['DB_USERNAME'], $config['DB_PASSWORD']);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		 if (isset($_POST['students'])) {
		 		$sortRIN = $conn->query('SELECT * FROM students ORDER BY rin ASC');
		 		printf("Sorted RIN:<br>");
        foreach ($sortRIN as $row) {
        	printf("%d: %s, %s<br>", $row['rin'], $row['lname'], $row['fname']);
       }
		 		$sortLNames = $conn->query('SELECT * FROM students ORDER BY lname ASC');
		 		printf("<br><br>Sorted Last Names:<br>");
        foreach ($sortLNames as $row) {
        	printf("%s, %s<br>", $row['lname'], $row['fname']);
       }
       	$sortFNames = $conn->query('SELECT * FROM students ORDER BY fname ASC');
       	printf("<br><br>Sorted First Names:<br>");
        foreach ($sortFNames as $row) {
        	printf("%s %s<br>", $row['fname'], $row['lname']);
       }

    } else {
        
    }

		
	} catch(PDOException $e) {
		echo 'ERROR: ' . $e->getMessage();
	}












?>