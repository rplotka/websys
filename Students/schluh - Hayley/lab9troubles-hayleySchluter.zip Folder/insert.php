<?php 


   require 'config.php';

   try {
      $conn = new PDO('mysql:host=localhost;dbname=websyslab9hayley', $config['DB_USERNAME'], $config['DB_PASSWORD']);
      $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

      if (isset($_POST['course'])){
        $i = 0;
        foreach ($_POST['crn'] as $row => $crn) {
          if (!empty($crn)) {
            $crn=$_POST['crn'][$row];
            $prefix=$_POST['prefix'][$row];
            $num=$_POST['num'][$row];
            $title=$_POST['title'][$row];
            $section=$_POST['section'][$row];
            $year=$_POST['year'][$row];

            $newcourse = "INSERT INTO courses (crn, prefix, num, title, section, year) VALUES ('$crn','$prefix','$num', '$title','$section', '$year')";
            $conn->exec($newcourse);
            $i++;
          }
        }
        echo "$i Course Inserted.";
      }
      
      if (isset($_POST['students'])){
        $i = 0;
        foreach ($_POST['rin'] as $row => $rin) {
          if (!empty($rin)) {
            $rin=$_POST['rin'][$row];
            $fname=$_POST['fname'][$row];
            $lname=$_POST['lname'][$row];
            $address1=$_POST['address1'][$row];
            $address2=$_POST['address2'][$row];
            $city=$_POST['city'][$row];
            $state=$_POST['state'][$row];
            $zip=$_POST['zip'][$row];
            $zip4=$_POST['zip4'][$row];

            $newstudents = "INSERT INTO students (rin, fname, lname, address1, address2, city, state, zip, zip4) VALUES ('$rin', '$fname', '$lname', '$address1', '$address2', '$city', '$state', '$zip', '$zip4')";
            $conn->exec($newstudents);
            $i++;
          }

        }
        echo "$i Students Entered";
      }

      if (isset($_POST['grades'])){
        $i = 0;
        foreach ($_POST['crn'] as $row => $crn) {
          if (!empty($crn)) {
            $id = $i;
            $rin=$_POST['rin'][$row];
            $crn=$_POST['crn'][$row];
            $grade=$_POST['grade'][$row];

            $newgrades = "INSERT INTO grades (id, rin, crn, grade) VALUES ('$id', '$rin', '$crn', '$grade')";
            $conn->exec($newgrades);
            $i++;
          }

        }

        echo "$i Grades Entered";
      }

   } catch(PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
   }
 ?>