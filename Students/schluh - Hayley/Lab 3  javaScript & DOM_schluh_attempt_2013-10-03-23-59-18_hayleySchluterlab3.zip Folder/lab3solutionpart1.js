root = document.getElementsByTagName('html')[0];
  var str = domIterate(root);
  document.getElementById('info').innerHTML = str;
  
  function domIterate(current, depth){
    if (!depth){
      depth = 0;
    }
    if (current.nodeType == 1){
      var txt = '';
      for (var i = 0; i < depth; i++){
        txt+= '-';
      }
        //add the element's tag name to the end, after indicating depth
        txt+=current.tagName + "\n";
      
        //for each child node, get the text of the child node
        for (var n = 0; n < current.childNodes.length; n++){
          childTxt = domIterate(current.childNodes[n], depth+1);
          //if the child node can be represented by text, add it to this one
          if (childTxt != null && childTxt != ''){
            txt+=childTxt;
          }
        }
        return txt;
      }
      else {
        return null;
      }
}