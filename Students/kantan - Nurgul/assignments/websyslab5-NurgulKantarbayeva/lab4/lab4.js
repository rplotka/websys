$("#coverart").live('click',function(){
	alert("It's working!");
	
	$.ajax({
		method: "GET",
		url: "lab4.json",
		dataType: "json",
	  	success: function(responseData,status) {
	  		$('#coverart').remove();
	  		var output = '';

	   	 	$.each(responseData.MyPlaylist, function(i,item) {
	   	 		output += '<ul><li class="songTitle">' + item.Title + "</li>";
	   	 		output += '<li class = "artist">' + item.Artist + "</li>";
	   	 		output += '<li class = "album">' + item.Album + "</li>";
                output += '<li class = "date">' + item.ReleaseDate + "</li>";
                output += '<li class = "genre"><ul>';
                $(item.Genres).each(function(i,gen) {
                	output += "<li>" + gen.Genre + "</li>";
                });
                output += "</ul>";
                output += '<li><a href = "' + item.Website + '">Official Website</a></li>';
                output += '<li class = "cover"><img src = "' + item.AlbumCover + '" alt = "Album Cover"></li>';
                output += "</ul>";
               });
	   	 	
	   	 	$(".listsong").html(output);
			$("#end").html('End of List');


			}, error: function(msg) {
			      alert("There was a problem: " + msg.status + " " + msg.statusText);
			}
	   	 	
	   	 });
});
