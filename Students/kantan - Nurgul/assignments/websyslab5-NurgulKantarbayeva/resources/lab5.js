$(document).ready(function() {
  
	
  	$.ajax({
   	 	type: "GET",
   	 	url: "resources/projects.xml",
   	 	dataType: "xml",
   	 	success: function(responseData, status){
   	  	var output = '';  
    	 	$(responseData).find("project").each(function() {
          output += '<h3>'+$(this).find("title").text()+ '</h3>';
          output += '<div><p>'+$(this).find("description").text()+ '</p>';
          output+= '<ul>'
          $(this).find("part").each(function() {
            output += '<li><a href="' + $(this).find("sublink").text()+'" target= "_blank">';
            output += $(this).find("subtitle").text()+'</a></li>';
          });
          output += "</ul></div>";
          $('#project').html(output).accordion("refresh");
      	});
      
      	
      	
    	}, error: function(msg) {
      				// there was a problem
      	alert("There was a problem: " + msg.status + " " + msg.statusText);
    	}
  	});
    
    
  });

$(function() {
      $("#project").accordion();
    });
      
    
	