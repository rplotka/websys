<?php
require 'config.php';

  
  try {
    //connecting to database
	$conndb = new PDO('mysql:host=localhost;dbname=websyslab9', $config['DB_USERNAME'], $config['DB_PASSWORD']);
	if ($conndb) {
	  echo "Connected to database!<br>";
	}
   //Checking to see what button is pressed and performing action accordingly

    if (isset($_POST['install']) && $_POST['install'] == 'Install') {
	   ob_start();
		require 'install.php';
		$output = ob_get_clean();
		echo $output; //only outputs install echo statements without executing all the queries
		
    }

    if (isset($_POST['load']) && $_POST['load'] == 'Load') {
    	require 'insert.php'; //executes everything in the insert file 
    }

    if (isset($_POST['students']) && $_POST['students'] == 'Students') {
    	require 'output.php'; //executes everything in the output file
    	foreach ($list_s as $row) {//prints output of our query in as an array
			echo '<pre>';
			print_r($row);
			echo '</pre>';

			printf("Last name = %s", $row['last_name']); //prints last name that is identified in an array
		}
    }


    if (isset($_POST['grdist']) && $_POST['grdist'] == 'Grade Distribution') {
    require 'output.php';
    echo '90-100: ';
		echo '<pre>';
		print_r($row_a); 
		echo '</pre>';
		echo '80-89: ';
		echo '<pre>';
		print_r($row_b);
		echo '</pre>';
		echo '70-79: ';
		echo '<pre>';
		print_r($row_c);
		echo '</pre>';
		echo '65-69: ';
		echo '<pre>';
		print_r($row_d);
		echo '</pre>';
		echo '<65: ';
		echo '<pre>';
		print_r($row_f);
		echo '</pre>';
		
    }
    
  




  }
  catch (Exception $e) {
    $err[] = $e->getMessage();
  }
?>

<!doctype html>
<html>
<head>
<title>Lab 9</title>
</head>
<body>
  
  <form method="post" action="index.php">
    <input type="hidden" name="id" value="$key" />
    <br/>
    <!-- Only one of these will be set with their respective value at a time -->
    <input type="submit" name="install" value="Install" />  
    <input type="submit" name="load" value="Load" />  
    <input type="submit" name="students" value="Students" />  
    <input type="submit" name="grdist" value="Grade Distribution" />  
  </form>
</body>
</html>

