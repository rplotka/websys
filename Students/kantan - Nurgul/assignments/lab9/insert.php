<?php
	
	$insert_c = "INSERT INTO `courses` (`crn`, `prefix`, `numb`, `title`, `section`, `year`) VALUES
	(43562, 'STSH', 1100, 'Science, Technology and Society', 2, 2014)";
	$insert_c=trim($insert_c);


	$insert_s = "INSERT INTO `students` (`rin`, `first_name`, `last_name`, `Address1`, `Address2`, `city`, `state`, `zip`,`zip-4`) VALUES
	(626663426, 'Sam', 'Riley', '2561 Burdett avenue','', 'Troy', 'NY', 12180,NULL),
	(661113454, 'Matthew', 'Roberts', '451 Broadway street apt. 11','', 'Troy', 'NY', 12180, NULL),
	(661122552, 'Jessica', 'Bell', '110 Tibbets avenue ','', 'Troy', 'NY', 12180, NULL),
	(662233541, 'Bill', 'Simpson', '56 11th street','', 'Troy', 'NY', 12180, NULL),
	(661122331, 'Thomas', 'Carter', '110 8th street','', 'Troy', 'NY', 12180, NULL),
	(657836842, 'Mary', 'Love', '213 4th street', 'apt. 15', 'Troy', 'NY', 12180, NULL),
	(662358365, 'Lucy', 'In the SKY', 'Beatles Avenue','', 'Strawberry Fields', 'Nowhere', 10000, NULL ),
	(123586542, 'Anna', 'Frank', '77 11th street','apt. B', 'Troy', 'NY', 12180, 2342),
	(665945916, 'Tim', 'Hanks', 'tired','', 'Troy', 'NY', 12180, 4244),
	(662235251, 'Elton', 'John', '125 Regent street','', 'London', 'UK', 12180, 2422)"; 
	$insert_s = trim($insert_s);
	

	$insert_g = "INSERT INTO  `grades` (`id` ,`rin` ,`crn` ,`grade`) VALUES 
	('0',  '123586542',  '43562',  '78'), 
	(NULL ,  '626663426',  '43562',  '98'), 
	(NULL ,  '657836842',  '43562',  '87'),
	(NULL ,  '661113454',  '43562',  '74'), 
	(NULL ,  '661122331',  '43562',  '58'),
	(NULL ,  '661122552',  '43562',  '93'), 
	(NULL ,  '662233541',  '43562',  '97'), 
	(NULL ,  '662235251',  '43562',  '88'),
	(NULL ,  '662358365',  '43562',  '90'),
	(NULL ,  '665945916',  '43562',  '91')";
	$insert_g =trim($insert_g);
	
