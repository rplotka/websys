<?php

require 'config.php';
try {
      $conn = new PDO('mysql:host=localhost', $config['DB_USERNAME'], $config['DB_PASSWORD']);
      if ($conn) {
      echo "Connected!<br/>";
   }

   } catch(PDOException $e) {
      echo 'ERROR: could not connect' . $e->getMessage();
   }

   

$conn->exec('CREATE DATABASE IF NOT EXISTS websyslab9 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci');

try {
      $conndb = new PDO('mysql:host=localhost;dbname=websyslab9', $config['DB_USERNAME'], $config['DB_PASSWORD']);
      if ($conndb) {
      echo "Connected to database!<br>";
   }
	} catch(PDOException $e) {
      echo 'ERROR: could not connect' . $e->getMessage();
   	}


   $courses="CREATE TABLE IF NOT EXISTS `courses`(
	`crn` int(11),
	`prefix` varchar(4) NOT NULL,
	`numb` smallint(4) NOT NULL, 
	`title` varchar(255) NOT NULL, 
	`section` varchar(255) NOT NULL,
	`year` int(4), 
	PRIMARY KEY(`crn`)
	) ENGINE=InnoDB DEFAULT CHARSET = utf8;";
	$courses=trim($courses);
	$status=$conndb->exec($courses); 
	echo 'Table Courses created='.$status.'<br>';

	$students="CREATE TABLE IF NOT EXISTS `students`(
	`rin` int(9),
	`first_name` varchar(100) NOT NULL,
	`last_name` varchar(100) NOT NULL, 
	`Address1` varchar(255) NOT NULL, 
	`Address2` varchar(255) NOT NULL,
	`city` varchar(100) NOT NULL, 
	`state` varchar(255) NOT NULL,
	`zip` int(5) NOT NULL,
	`zip-4` int(4) NULL,
	PRIMARY KEY(`rin`)
	) ENGINE=InnoDB DEFAULT CHARSET = utf8;";
	$students=trim($students);
	$status=$conndb->exec($students); 
	echo 'Table Students created='.$status.'<br>';

	$grades="CREATE TABLE IF NOT EXISTS `grades`(
	`id` int(2) AUTO_INCREMENT,
	`rin` int(9),
	`crn` int(11), 
	`grade` int(3) NOT NULL, 
	FOREIGN KEY(`rin`) REFERENCES `students` (`rin`),
	FOREIGN KEY (`crn`) REFERENCES `courses` (`crn`), 
	PRIMARY KEY(`id`)
	) ENGINE=InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT=1;";
	$grades=trim($grades);
	$status=$conndb->exec($grades); 
	echo 'Table Grades created='.$status.'<br>';





	
 ?>