Each of my classes have two methods: operate and getEquation
"operate" conducts operations with parameaters passed to the class. So if for example class is
Multiplication operate multiplies operand1 and operand2 that are passed when a new class object
is declared.
"getEquation" puts these operands passed to a class object and the result of operate function call
into a string.
When one of the buttons is clicked the operands posted in the boxes are assigned to values that
are going to be passed to the class upon declaration.
Then we check to see to what _POST was set and accordingly we create a class object assigning it 
to $op value. Then we check if $op is set and if it is is we call up on one of our classes getEquation
method to output on the page.

It is best practice to use post to send form data. Also with GET method all variables are 
visible in the URL. It doesn't really matter in this case I guess because we're not sending 
any sensitive data.

Another way to determine which button has been pressed and take the appropriate action is to use switch
operator. Declaring the cases for each button and what to do when it is pressed. 