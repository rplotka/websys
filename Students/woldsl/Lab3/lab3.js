// Get the root of the document.
var root = document.getElementsByTagName('html')[0];

// Create a variable to hold our text-based tree.
var quote = null;
var tmp = root.getElementsByTagName('div');
quote = tmp[0].cloneNode();
root.appendChild(quote);
var string = domIterate(root);
document.getElementById('info').innerHTML = string;

function domIterate(current, depth) 
{
	// Set the depth if not done so already.
	if (!depth) 
	{
		depth = 0;
	}

	// If this is an element node:
	if (current.nodeType == 1) 
	{
		// Start building node text.
		var txt = '';

		// One dash per point of depth.
		for (var i = 0; i < depth; i++) 
		{
			txt += '-';
		}

		// Add the element's tag name to the end after the dashes.
		txt += current.tagName + "\n";

		// Bind the onclick handler. Not very efficient.
		current.onclick = function(e)
		{
			alert(this.tagName);
		}

		if (current.tagName == "DIV") 
		{
			current.onmouseover = function()
			{
				current.setAttribute("style", "padding-left:10px");
				current.style.backgroundColor = "red";
			};
			current.onmouseout = function()
			{
				current.setAttribute("style", "padding-left:0px");
				current.style.backgroundColor = "white";
			};
		};

		// For each child node:
		for (var i = 0; i < current.childNodes.length; i++) 
		{
			// Get the text of the child node.
			var childTxt = domIterate(current.childNodes[i], depth+1)

			// If the child node can be represented by text, add it to this one.
			if (childTxt != null && childTxt != '') 
			{
				txt += childTxt;
			}
		}
		return txt;
	}
	// Otherwise return null. (End case.)
	else
	{
		return null;
	}
}
