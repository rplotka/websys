Logan Shire
Web Systems Development
Homework 1
9/26/13

(My files are the three html pages, index.html, contact-me.html, and coming-soon.html
as well as the stylesheet.css and LoganLogo.png file.)

Why did you select the doctype you used?
	I used HTML because I am most familiar with it and it seemed best suited to this kind of simple, static web page.

How does your doctype compare to other commonly seen (strict) doctypes?
	HTML is different than other doctypes like XML in that it was specifically written to describe presentation. As such it has orders of magnitude more flexibility when it comes to laying out a page. XML simply cannot offer that degree of flexibility.

How is the markup for your document semantically correct? Are there any non-semantic elements that you needed for styling purposes?
	My markup is entirely semantically correct. I never used any HTML styling tags, deferring to CSS either through my own stylesheet or Bootstrap. The divs I used
	were either required by Bootstrap to to tag elements with classes or by hcard
	to identify relevant information.

In your own words, how is the hCard information you added useful to both humans and automated user agents trying to access your content?
	It is useful to crawlers as it neatly tags the relevant information making sure
	they don't miss anything, and useful to humans as it makes it easier to identify
	and display contact details.