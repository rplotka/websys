root = document.getElementsByTagName('html')[0];

var str = domIterate(root);
document.getElementById("info").innerHTML = str;

function domIterate(current, depth) {
	if (!depth) {
		depth = 0;
	}
	
	if (current.nodeType == 1) {
		var txt = '';
		
		for (var i = 0; i < depth; i++) {
			txt += '-';
		}

		txt += current.tagName + "\n";

		// PART 2
		current.onclick = function(e) {
			alert(this.tagName);
		};
		// END PART 2
		
	
		
		for (var n = 0; n < current.childNodes.length; n++) {
			childTxt = domIterate(current.childNodes[n], depth+1);
		
			if (childTxt != null && childTxt != '') {
				txt += childTxt;
			}
		}
		return txt;
	}
	else {
		return null;
	}
}

// PART 3 IS HERE

oldNode=document.getElementsByTagName('div')[0];
newNode=oldNode.cloneNode(true);
document.documentElement.appendChild(newNode);

var divs=document.getElementsByTagName('div');

for(var i = 0; i < divs.length; i++) {
	var div = document.getElementsByTagName('div')[i];
	div.onmouseover=function() { 
		this.style.backgroundColor = "blue"; 
		this.style.position="relative";
		this.style.left="10px";
		
	}
	div.onmouseout=function() { 
		this.style.backgroundColor = "white"; 
		this.style.position="inherit";
		this.style.left="0px"; 
	}
}
