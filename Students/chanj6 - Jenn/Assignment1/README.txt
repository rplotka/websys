Jennifer Chan

- HTML4 is a Standard Generalized Markup Language. It has all the previous features (text, multimedia, and hyperlink), as well as more multimedia options, scripting languages, style sheets, and documents that are more accessible to users with disabilities. Because it is the strict version, it doesn't allow features that are considered bad form on the web.

- XHTML strict has to be written in well-formed xml. As for HTML5, I don't need all the multimedia capabilities.

- I use hcard for my contact information. It uses the right tags to label the information. However, I use a lot of divs to divide up different sections. 

- It makes web crawlers to easily indetify the information on my page. That would help them recognize that this is a resume with contact info.

======================================================================

Citation:
- hcard: http://en.wikipedia.org/wiki/HCard
- http://www.w3schools.com/tags/tag_doctype.asp
- http://www.w3.org/TR/REC-html40/
- http://en.wikipedia.org/wiki/Standard_Generalized_Markup_Language
- http://en.wikipedia.org/wiki/HTML5