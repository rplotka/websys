root = document.getElementsByTagName('html')[0];
var str = domIterate(root, 0);
document.getElementById('info').innerHTML = str;

//==================== Part 3: Copy Node =====================
var nodeCopy = document.getElementsByClassName('quote');
var newNode = nodeCopy[0].cloneNode(true);
document.getElementsByTagName('body')[0].appendChild(newNode);
//==================== Part 3: mouse over =====================
var div = document.getElementsByClassName( 'quote' )[0];
div.onmouseover = function() {
  this.style.backgroundColor = 'green';
};
div.onmouseout = function() {
  this.style.backgroundColor = 'transparent';
};

var div1 = document.getElementsByClassName( 'quote' )[1];
div1.onmouseover = function() {
  this.style.backgroundColor = 'green';
};
div1.onmouseout = function() {
  this.style.backgroundColor = 'transparent';
};

var div2 = document.getElementsByClassName( 'quote' )[2];
div2.onmouseover = function() {
  this.style.backgroundColor = 'green';
};
div2.onmouseout = function() {
  this.style.backgroundColor = 'transparent';
};

var div3 = document.getElementsByClassName( 'quote' )[3];
div3.onmouseover = function() {
  this.style.backgroundColor = 'green';
};
div3.onmouseout = function() {
  this.style.backgroundColor = 'transparent';
};

var div4 = document.getElementsByClassName( 'quote' )[4];
div4.onmouseover = function() {
  this.style.backgroundColor = 'green';
};
div4.onmouseout = function() {
  this.style.backgroundColor = 'transparent';
};

function  domIterate(current, depth) {
	if (current.nodeType == 1) {
		var txt = '';

		for (var i = 0; i < depth; i++) {
			txt += '-';
		}

		txt += current.tagName + "\n";

		//============== part 2 ================
		current.onclick = function(e) {
			alert(this.tagName);
		}
		//=======================================
		
		for (var n = 0; n < current.childNodes.length; n++) {
			childTxt = domIterate(current.childNodes[n], depth+1);

			if (childTxt != null && childTxt != '') {
				txt += childTxt;
			}
		}
		return txt;
	}
	else {
		return null;
	}
}
