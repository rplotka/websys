root = document.getElementsByTagName('html')[0];

var str = domIterate(root);
document.getElementById('info').innerHTML = str;

function domIterate(current, depth){
	if (!depth){
		depth = 0;
	}
	
	if (current.nodeType == 1){
		var txt = '';
		
		for (var i = 0; i<depth;i++){
			txt += '-';
		}

		txt += current.tagName + '\n';

		current.onclick = function(e){
			alert(this.tagName);
		};

		for (var n=0;n<current.childNodes.length;n++){
			childTxt = domIterate(current.childNodes[n],depth+1);

			if (childTxt != null && childTxt!= ''){
				txt += childTxt;
			}
		}
		return txt;
	}
	else {
		return null;
	}
}

var node = document.getElementsByTagName("div")[0].cloneNode(true);
document.getElementsByTagName('body')[0].appendChild(node);

var divs = document.getElementsByTagName("div");



for (var n=0;n<divs.length;n++){
	divs[n].onmouseover = function(e){
		this.style.backgroundColor = "#ffff00";
		this.style.position = 'relative';
		this.style.left = '10px';
	}
	divs[n].onmouseout =function (e){
		this.style.backgroundColor = "#ffffff";
		this.style.left = '0px';
	}
}