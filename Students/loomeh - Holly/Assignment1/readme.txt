Holly Loomer

o	Why did you select the doctype you used?
I chose XHTMl 1.0 strict because it is more closely associated with XML. The hCard microformat is used for publishing details in XHTML, atom, RSS, or XML, and so it made the most sense to use XHTML for this assignment's other task.


o	How does your doctype compare to other commonly seen (strict) doctypes?
My doctype must be written as well-formed XML whereas HTML 4.01 strict does not.


o	How is the markup for your document semantically correct? Are there any non-semantic elements that you needed for styling purposes? If you used divs and /or spans, be sure to account for them in your explanation.

My markup is semantically correct because I used the correct tags for my correct purposes. An example is of where I used h2, h4 for headers. This helped me differentiate from other, regular text, and gave me the oportunity to apply style to the headers easily as well. I also used unordered list because a lot of my elements would be in a list format (skills, tasks for work experience, high school accomplishments).
Spans and divs were used for the hCard Microformat in order to properly label the information. I also used divs for styling purposes. The divs were a way to seperate the headers from their corresponding paragraphs/information. This way I could float items to the left and right, and I was able to make columns this way.


o	In your own words, how is the hCard information you added useful to both humans and automated user agents trying to access your content?

It is useful because for users they can detect the hCard and if they so choose, easily download the contact info as a whole instead of copying and pasting each individual peice of my information.
For automated user agents, parsing tools can extract the details fast and display them using other appliations or tools in order to be loaded into another space.


REFERENCES:

Sublime Help Guide:
-https://tutsplus.com/course/improve-workflow-in-sublime-text-2/

-www.w3.org

-resume from Intro to IT as a basis; improved upon it from last year and added requirements for this year

-Questions asked and talked about with Candice Poon

-Plotka (email)