

//start here
start = document.getElementsByTagName('html')[0];

//string points to this function
var string = iterateDOM(start);

//replace "info" id with what this function spits out
document.getElementById('info').innerHTML = string;


//cloning to the bottom of the document
var node = document.getElementsByTagName('div')[0]; //children of the body
var clone = node.cloneNode(true);
document.body.appendChild(clone);

//onmouseover event listeners: shifting div, diff background color
var divs = document.getElementsByTagName('div');
for (var i=0; i<divs.length; i++) {
  divs[i].onmouseover = function() {
    this.style.backgroundColor="#f3f3f3";
    this.style.paddingLeft="10px";
  }
  divs[i].onmouseout = function() {
    this.style.backgroundColor="#ffffff";
    this.style.paddingLeft="0px";
  }
}

function iterateDOM(current, depth) {
  if (!depth) {  //how far into the tree we go
  	depth=0; 
  }

 //permanently assigned nodetype values (html=1)
  if (current.nodeType == 1) {
  	var txt = '';

  	for (var i=0; i<depth; i++) {
  		txt += '-';
  	}

  	txt += current.tagName + "\n";

  	current.onclick=function() {    //alert of the tag
  	    alert(current.tagName);
  	}

  	for (var n=0; n<current.childNodes.length; n++) {
  		childTxt = iterateDOM(current.childNodes[n],depth+1);


  		if (childTxt != null && childTxt != '') {
  			txt += childTxt;
  		}

  	}

  	return txt;
  }
  else {
  	return null;
  }

}