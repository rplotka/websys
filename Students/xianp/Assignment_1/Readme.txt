Assignment 1
Author: Pinyuan(Doris) Xian

Part 1: Markup & CSS

All the links are invisible, but if you hover on it, there is underline to suggest you that it is a link.
In this resume: Emails, Name of Schools and Companys are all links.
I made my emails in a different color to attract attention.
As for schools and companys, I think recruiter most likely don't need to go to the webpage. However, if they want, I do provide an easy link to get there.
* I use target="_blank" to make the linked webpage open in a new tab/window instead of the current time (HTML5)


Part 1b: Microformats

I use the hCard microformat in header for contact info section.
I have done the multi-browsers tests using BrowserStack.
I use the W3C validator to check my HTML and CSS files. 

-Why did you select the doctype you used?
I use the new HTML 5 DOCTYPE because it is the shortest doctype and yet still trigger standards mode on all browsers.

-How does your doctype compare to other commonly seen (strict) doctypes?
It is small and it supports new semantic elements in HTML5.

-How is the markup for your document semantically correct? Are there any non-semantic elements that you needed for styling purposes? If you used divs and /or spans, be sure to account for them in your explanation.
I use semantic tags and elements as well as some new semantic elements offered by HTML5 to clearly define different parts of my webpage.
I use <div> tags to group together elements/sections to make the layouts clear and specifically format them with CSS. (eg. <div id="inner">)
I use <span> tag when there are inline-elements need to be presented in the page.(eg. <span class="dates">) 

-In your own words, how is the hCard information you added useful to both humans and automated user agents trying to access your content?
hCard made the contact information more structured. In this way, humans can easily reach and format the contact info by class. On the other hand, info can be understood by search robots that is very useful to the automated user agents to do crawling.


Challenge: Web Fonts & CSS3

I use Google Font Lato.
Some CSS3 features.(eg. border-radius)
I use MONO SOCIAL ICONS FONT by @font-face.
The rounded social icons matched with the rounded bordered picture. =)


References:
http://css-tricks.com/snippets/html/open-link-in-a-new-window/
http://drinchev.github.io/monosocialiconsfont/
http://www.w3schools.com/tags/

