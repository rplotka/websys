// Lab #3 - JavaScript and the Document Object Model
//	Authour: Pinyuan (Doris) Xian

root = document.getElementsByTagName('html')[0];
var str = domIterate(root);
document.getElementById('info').innerHTML = str;

function domIterate(current, depth) {
	if (!depth){
		depth = 0;
	}
	if (current.nodeType == 1){
		var txt = '';
		for (var i = 0; i < depth; i++) {
			txt += '-';
		}
		txt += current.tagName + "\n";

		//Part 2
		current.onclick = function(e){
			alert(this.tagName);
		};

		for(var n = 0; n < current.childNodes.length; n++) {
			childTxt = domIterate(current.childNodes[n], depth+1);
			if(childTxt != null && childTxt !=''){
				txt += childTxt;
			}
		}
		return txt;
	}
	else{
		return null;
	}
};

// Part 3
// copy quote ( Part 0) to the end of the body
var q = document.getElementsByTagName('div')[0].cloneNode(true);
document.body.appendChild(q);

// add onmouseover and onmouseout event listeners to each div
var obj = document.getElementsByTagName('div');
for (var i=0; i < obj.length; i++) {
	// onmouseover: change the element's background color and move the element 10px to the right
	obj[i].onmouseover = function(){
		this.style.backgroundColor = "lightblue";
		this.style.paddingLeft = "10px";
	}
	// onmouseout: change the background color back to white and reposition the element to its original location
	obj[i].onmouseout = function() {
      	this.style.backgroundColor = "white";
      	this.style.paddingLeft = "0";
    }
}