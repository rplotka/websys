<?php 

//make class operation and add the two variables to substitute in for math as operands
  abstract class Operation {
    protected $operand_1;
    protected $operand_2;
    public function __construct($o1, $o2) {
      // Make sure we're working with numbers...
      if (!is_numeric($o1) || !is_numeric($o2)) {
        throw new Exception('Non-numeric operand.');
      }
      
      $this->operand_1 = $o1;
      $this->operand_2 = $o2;
    }
    public abstract function operate();
    public abstract function getEquation(); 
  }

  //added same types of classes for each including subtraction, multiplication and division, except change the way formulas are
  class Addition extends Operation {
    public function operate() {
      return $this->operand_1 + $this->operand_2;
    }
    public function getEquation() {
      return $this->operand_1 . ' + ' . $this->operand_2 . ' = ' . $this->operate();
    }
  }

  class Subtraction extends Operation {
   public function operate() {
    return $this->operand_1 - $this->operand_2;
  }
  public function getEquation() {
    return $this->operand_1 . ' - ' . $this->operand_2 . ' = ' . $this->operate();
  }
}

  class Multiplication extends Operation {
   public function operate() {
    return $this->operand_1 * $this->operand_2;
  }
  public function getEquation() {
    return $this->operand_1 . ' * ' . $this->operand_2 . ' = ' . $this->operate();
  }
}

  class Division extends Operation {
   public function operate() {
    return $this->operand_1 / $this->operand_2;
  }
  public function getEquation() {
    return $this->operand_1 . ' / ' . $this->operand_2 . ' = ' . $this->operate();
  }
}


// Part 1 - Add subclasses for Subtraction, Multiplication and Division here



// End Part 1




// Some debugs - un comment them to see what is happening...
// echo '$_POST print_r=>',print_r($_POST);
// echo "<br>",'$_POST vardump=>',var_dump($_POST);
// echo '<br/>$_POST is ', (isset($_POST) ? 'set' : 'NOT set'), "<br/>";
// echo "<br/>---";




// Check to make sure that POST was received 
// upon initial load, the page will be sent back via the initial GET at which time
// the $_POST array will not have values - trying to access it will give undefined message

  if($_SERVER['REQUEST_METHOD'] == 'POST') {
    $o1 = $_POST['op1'];
    $o2 = $_POST['op2'];
  }
  $err = Array();


// Part 2 - Instantiate an object for each operation based on the values returned on the form
//          For example, check to make sure that $_POST is set and then check its value and 
//          instantiate its object
// 
// The Add is done below.  Go ahead and finish the remiannig functions.  
// Then tell me if there is a way to do this without the ifs

  try {
    if (isset($_POST['add']) && $_POST['add'] == 'Add') {
      $op = new Addition($o1, $o2);
    }
      }
  catch (Exception $e) {
    $err[] = $e->getMessage();
  }
// Put the code for Part 2 here  \/
// End of Part 2   /\ 
if($_POST) {
  //get variables from the POST and form them to test
  if(isset($_POST["add"])) {
    $a = new Addition ($_POST["op1"], $_POST["op2"]);
  }
  else if(isset($_POST["sub"])) {
    $a = new Subtraction ($_POST["op1"], $_POST["op2"]);
  }
  else if(isset($_POST["mult"])) {
    $a = new Multiplication ($_POST["op1"], $_POST["op2"]);
  }
  else if(isset($_POST["div"])) {
    $a = new Division ($_POST["op1"], $_POST["op2"]);
  }

  $result = $a->getEquation();
  echo $result;

}  
?>

<!doctype html>
<html>
<head>
<title>Madi Vastare-Lab 7</title>
</head>
<body>
  <pre id="result">
  <?php 
    if (isset($op)) {
      try {
        echo $op->getEquation();
      }
      catch (Exception $e) { 
        $err[] = $e->getMessage();
      }
    }
      
    foreach($err as $error) {
        echo $error . "\n";
    } 
    //post the bottom as form to post
  ?>
  </pre>
  <form method="post" action="lab7start.php">
    <input type="text" name="op1" id="name" value="" />
    <input type="text" name="op2" id="name" value="" />
    <br/>
    <!-- Only one of these will be set with their respective value at a time -->
    <input type="submit" name="add" value="Add" />  
    <input type="submit" name="sub" value="Subtract" />  
    <input type="submit" name="mult" value="Multiply" />  
    <input type="submit" name="div" value="Divide" />  
  </form>
</body>
</html>

<!--I have different classes which include the operations of adding, subtracting, multiplication, and division through an if statement and add the different variables from the input. If I were to use the GET statement, it would recieve the variable for the operation. I Personally think the if statement was the easiest way to carry out the operations. -->





