�	Your name
Madi Vastare


o	Why did you select the doctype you used?

I used HTML 5, because I had prior experience with it. 

o	How does your doctype compare to other commonly seen (strict) doctypes?
I have heard it is newer and easier to use than prior/older doctypes. 

o	How is the markup for your document semantically correct? Are there any non-semantic elements that you needed for styling purposes? If you used divs and /or spans, be sure to account for them in your explanation.
I believe my markup for the document I made is semantically correct, because I closed all formations I had created. I also used proper indentation. I used spans for hCard microformatting, because I felt it would be the easiest way to style inline text. I took an easy route by creating lists for translating data for the resume, because it was the only way I truly understood.  

o	In your own words, how is the hCard information you added useful to both humans and automated user agents trying to access your content?
It is useful because it is standard, and is able to be translated efficiently and easily by computers and people.

�	A list of any and all works referenced throughout the assignment, including tutorials, reference sheets, etc. Failure to cite all referenced works may result in a zero for the assignment and a two-letter drop in your final grade. When in doubt, email the instructor or TA.

http://www.csszengarden.com/ # for inspiration
http://www.w3schools.com/css/ # for help on css
http://microformats.org/wiki/hcard #for hcard help
http://www.google.com/webfonts #fonts

