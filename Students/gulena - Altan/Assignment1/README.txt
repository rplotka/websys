Name: Altan Gulen

Why did you select the doctype you used?
	I selected XHTML 1.0 strict because I am not using any deprecated elements/attributes. 

How does your doctype compare to other commonly seen (strict doctypes)?
	XHTML 1.0 Strict and HTML 4.01 Strict differ in preparedness for the future and syntax. For example, In XHTML, all
	elements and attributes appear in lower-case and all attributes must be quoted. The margin for error in
	HTML 4.01 is greater without these rules.

How is the markup for your document semantically correct? Are there any non-semantic elements that you needed for styling purposes? If you used divs and/or span, be sure to account for them in your explanation.
	The markup for my document is semantically correct because every div/span tag serves a specific purpose. The divs
	are used for styling the document and making sure the spacing is correct. The span tags are used in the
	hCard integration.

In your own words, how is the hCard information you added useful to both humans and automated user agents trying to access your content? 
	hCard information can be converted by web crawlers and other aggregators into contact information, which is good
	for automated user agents. For humans, the hCard data can be downloaded instead of forcing the user to copy/paste each individual piece of contact info.

References:
	W3C - http://www.w3schools.com/ (general property look up)
	W3C Color Picker - http://www.w3schools.com/tags/ref_colorpicker.asp
	W3C HTML Validator - http://validator.w3.org/
	W3C CSS Validator - http://jigsaw.w3.org/css-validator/validator
	hCard Wiki - http://microformats.org/wiki/hcard