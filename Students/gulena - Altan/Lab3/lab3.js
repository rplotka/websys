window.onload = function()
{
	root = document.getElementsByTagName('html')[0];
	var str = parseHtmlTree(root, 0);
	document.getElementById('info').innerHTML = str;
	appendQuote();
};

//Appends first quote div to the end of the document
function appendQuote()
{
	var clonedNode = document.getElementsByTagName("div")[0].cloneNode(true);
	document.getElementsByTagName("div")[3].appendChild(clonedNode);
}

//Iterates through DOM and creates string showing tree structure
function parseHtmlTree(currentNode, numDashes)
{
	//If current node is an element tag...
	if(currentNode.nodeType == 1)
	{
		var text = '';

		for(var i = 0; i < numDashes; i++)
		{
			text += '-';
		}
		text += currentNode.tagName + "\n";
		
		currentNode.onclick = function(e) {
			alert(this.tagName);
		};

		//Recurse through every child of current node
		for(var childCount = 0; childCount < currentNode.childNodes.length; childCount++)
		{
			childText = parseHtmlTree(currentNode.childNodes[childCount], numDashes + 1);

			if(childText != null && childText != '')
			{
				text += childText;
			}
		}

		return text;
	}
	else
	{
		return null;
	}
}

function mouseOver(node)
{
	node.style.backgroundColor = "red";
	node.style.paddingLeft = "10px";
}

function mouseOut(node)
{
	node.style.backgroundColor = "white";
	node.style.paddingLeft = "0px";
}