function lab() {
  Part1();
  // Part 2 is a part of part 1
  Part3();
}

function Part1() {
  var output = listing(document.getElementsByTagName("html")[0], 0);
  document.getElementById("info").innerHTML = output;
  window.clicking = 0;
}

function listing(current, level) {
  var output = "",
      nextlevel = level + 1,
      kids = current.children,
      kid, len, i;
  for(i = level - 1; i >= 0; --i) 
    output += "-";
  output += current.tagName.toLowerCase();
  
  if(kids && kids.length > 0)
    for(len = kids.length, i = 0; i < len; ++i) {
      kid = kids[i];
      kid.onclick = part2alert;
      output += "\n" + listing(kid, nextlevel);
    }
  
  return output;
}

function part2alert(event) {
  if(window.clicking) return;
  ++window.clicking;
  alert(event.target.tagName.toLowerCase());
  setTimeout(function() { --window.clicking; });
}

function Part3() {
  document.body.appendChild(document.getElementsByClassName("quote")[1].cloneNode(true));
  
  var elements = document.getElementsByTagName("div"),
      kid, i;
  for(i = elements.length - 1; i >= 0; --i) {
    kid = elements[i];
    kid.onmouseover = part3over;
    kid.onmouseout = part3out;
  }
}

function part3over(event) {
  var style = event.target.style;
  style.backgroundColor = "#dfd";
  style.paddingLeft = "10px";
}

function part3out(event) {
  var style = event.target.style;
  style.backgroundColor = "";
  style.paddingLeft = "";
}

lab();