Albert Armea
============

Doctype
-------
I chose the <!DOCTYPE html> HTML5 doctype so that I could use modern
content-specific tags such as the article and footer tags. These tags are not
present in HTML4-based doctypes.

Semantic correctness
--------------------
I used spans and divs to format addresses cleanly and group the top header so
that it could be easily styled.

hCard information
-----------------
I followed the
[hResume microformat specification](http://microformats.org/wiki/hResume). This
microformat does not add extra information to the presentation of the resume to
humans. At the same time, it annotates all information in the resume, including
contact information as a hCard, experience and education as hCalendar events,
and skills marked with the `skill` class so that it can be easily machine parsed
and analyzed.

References
----------
These are all of the tabs I had open by the time I finished the assignment.

<http://microformats.org/wiki/hResume>
<http://www.devlounge.net/design/aligning-images-the-right-way-using-css>
<https://developer.mozilla.org/en-US/docs/Web/CSS/text-align>
<http://be-jo.net/2012/05/lebenslauf-mit-latex-mustervorlage/>
<http://dl.dropboxusercontent.com/u/4025249/stefanie_cv/cv_stefanie_v3photo.pdf>
<http://lesscss.org/>
<https://developer.mozilla.org/en-US/docs/Web/CSS/vertical-align>
<https://developer.mozilla.org/en-US/docs/Web/CSS/border-width>
<http://kevin.oconnor.mp/assets/resume/resume.pdf>
<http://stackoverflow.com/questions/13801917/getting-stray-end-tag-html-from-w3c-validator>
