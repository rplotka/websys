Web Systems Development 
Assignment 1
Name: Taha Mehdi

-The doctype selected was HTML5. A couple reasons why I chose this include:
1. New semantic elements such as header and section allow for an easier understanding of the page.
2. The future is HTML5, so it is good practice to start adapting to HTML5.
3. The implementation of the doctype is extremely simple compared to that of HTML4 or XHTML.

-Obviously HTML5 as mentioned above introduces new semantic elements to allow for better organization. In addition, there is greater emphasis on separating semantics from presentation in HTML 5, so elements such as font and center are now deprecated.

-Content in the resume is separated into different sections; for instance, the objective, education, skills, and work are all in different sections. 
Section id's were included even if no CSS was applied in order to allow for padding.
Only one div element was used, within the header section to center the name, photo, and contact information in the header.
Span elements were used in the resume:
a. Locality, region, and postal code for the addresses are of type span, in order to keep the elements inline. Furthermore because of the hCard microformatting, a class needed to be applied anyway to the element.
b. A class called name was implemented and was used for both p and span elements. Span elements were again used to keep the element inline.

-For automated agents such as search engines robots, microformats allow for finding particular elements in an easy fashion, because all it needs to do is find a specific markup (for example, with hCard formatting, street address is given by street-address).
For humans, the hCard information is useful, because certain browsers will detect that there is hCard markup and allow the user to download the entire contact information as opposed to manually copy and pasting individual elements of the contact information.

-Challenge: Web Fonts / CSS 3
For the challenge, I included an external link in my HTML referring to Google fonts. I included two fonts, Cantora One (sans-serif) and Libre Baskerville (serif). The default font was then changed to Libre Baskerville, while the names of the categories and my own name were changed to Cantora One. 
For CSS 3 elements, I included a few features.
a. Introduced a box around the resume, with rounded corners and drop shadow.
b. Introduced background with gradients.
c. Introduced feature where on hover over category name, the name gets bigger and kerning is increased.
d. Introduced feature where on hover on external links only (http) a text-shadow appears.
These all follow the concept of progressive enhancement. For instance, for the gradient background, if the browser does not support gradients it will revert to a solid color gradient. Similarly, if the browser cannot use the web font, then it will revert to the default serif or sans serif font.

Resources:
http://microformats.org/wiki/hcard-faq
http://microformats.org/wiki/hcard
http://www.w3schools.com/css3/css3_transitions.asp
http://net.tutsplus.com/tutorials/html-css-techniques/the-30-css-selectors-you-must-memorize/
http://www.cognifide.com/blogs/ux/5-reasons-why-html5-matters/
http://stackoverflow.com/questions/1294493/what-does-semantically-correct-mean
http://css3generator.com/
http://www.w3schools.com/cssref/pr_list-style-type.asp
http://www.google.com/fonts