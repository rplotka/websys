// Taha Mehdi
// Web Systems Development
// Lab 3

// Part 1 (mostly)
function htmlTree(obj, depth){
	// if depth not set (running first time, for example) then set to 0
	if(!depth) {
		depth = 0;
	}
	
	// if the object is an element...
	if(obj.nodeType == 1) {
		// build the text (- level and tag name)
		var txt = "";
		for (var i = 0; i < depth; i++) {
			txt += "-";
		}
		txt += obj.tagName + "\n";
		
		// Part 2 - add onclick element with alert
		obj.addEventListener('click', function() {alert(obj.tagName);},false);
		
		// For each of the child nodes perform the same function and concatenate
		// the text
		for (var n = 0; n < obj.childNodes.length; n++) {
			childTxt = htmlTree(obj.childNodes[n],depth+1);
			if (childTxt!= null && childTxt != "") {
				txt += childTxt;
			}
		}
		// return completed text
		return txt;
	}
	// if there are no elements just retun null
	else { 
		return null; 
	}
}

// Part 3 function
// On mouseover change background color and move element 10px to right.
function divMouseOver() {
	var cssChange = "background-color:red; padding-left: 10px;";
	this.style.cssText = cssChange;
}

// Part 3 function
// On mouseout get rid of CSS changes made from previous function
function divMouseOut() {
	var cssChange = "";
	this.style.cssText = cssChange;
}

// Part 1 - begin with the html tag and after function is complete, replace 
// the info div with the completed tree
var s = htmlTree(document.getElementsByTagName('html')[0]);
document.getElementById('info').innerHTML = s;


// Part 3a
// Get the first element with class quote and clone it
var itm = document.getElementsByClassName('quote')[0].cloneNode(true);
// Get the body element and append the clone to it
document.getElementsByTagName('body')[0].appendChild(itm);

// Part 3b
// Find all div elements, add mouseover and mouseout listeners
var elJ = document.getElementsByTagName("div");
for (var i=0;i<elJ.length;i++) {
	elJ[i].addEventListener('mouseover',divMouseOver,false);
	elJ[i].addEventListener('mouseout',divMouseOut,false);
}