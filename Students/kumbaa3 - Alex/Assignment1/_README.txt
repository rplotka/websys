Name: Alex Kumbar

Why did you select the doctype you used?
	I selected xhtml 1.0 strict because it would force me to use the most up to date conventions.

How does your doctype compare to other commonly seen (strict) doctypes?
	Xhtml strict is a combination of xml and html and is stricter and cleaner than Html 4.01 strict.

How is the markup for your document semantically correct? Are there any non-semantic elements that you needed for styling purposes? If you used divs and /or spans, be sure to account for them in your explanation.
	I found that using divs to distinguish the ul's from each other helped with centring and generally separating out the code. Id tags gave the divs semantic meaning and classes were used to format all but the education div. Spans were only used to add hCard tags.

In your own words, how is the hCard information you added useful to both humans and automated user agents trying to access your content?
	People can easily identify things like contact information by just opening up the code and using a computer search function for what they're looking for. An automated user would be able to quickly grab and aggregate the desired data, because it is labelled based on a convention.
	
	
	References:
	
	http://microformats.org/wiki/hcard
	
	http://www.w3schools.com/
	
	lecture notes and lab 2