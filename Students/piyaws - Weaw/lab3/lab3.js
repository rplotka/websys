
var root = document.getElementsByTagName("html")[0];

var str = domIterate(root);
document.getElementById("info").innerHTML = str;

// recursive function that iterates through the document
function domIterate(current, depth) {
	if (!depth) {
		depth = 0;
	}

	if (current.nodeType == 1) {
		var txt = '';

		for (var i = 0; i < depth; i++) {
			txt += '-';
		}

		txt += current.tagName + "\n";

		
		// part 2
		// e is just a place holder
		current.onclick = function(e) {
			alert(this.tagName);
		}
		// end of part 2
		

		for (var n = 0; n < current.childNodes.length; n++) {
			childTxt = domIterate(current.childNodes[n],depth+1);

			if (childTxt != null && childTxt != '') {
				txt += childTxt;
			}
		}
		return txt;
	}
	else {
		return null;
	}
}


// part3

// write the cloneNode at the end of the file
var node=document.getElementsByTagName("div")[1];
var cloneNode = node.cloneNode(true);
document.documentElement.appendChild(cloneNode);

// add event-listener to div tags
for (var i=0;i<document.getElementsByTagName("div").length;i++)
{ 
	var div=document.getElementsByTagName("div")[i];
	div.onmouseover = function() {
		var oldleft=div.offsetLeft;
		this.style.backgroundColor = '#FFCCCC';
		this.style.position = 'relative';
		this.style.right = '10px';
	}
	div.onmouseout = function() {
		this.style.backgroundColor = 'transparent';
		this.style.position = 'static';
		this.style.left = oldleft;
	}
}



