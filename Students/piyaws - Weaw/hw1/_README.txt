Name: Sakawkarn Piyawitwanich

Why did you select the doctype you used?
-It is easy to implement because I know how to manage with it.

How does your doctype compare to other commonly seen (strict) doctypes?
-My doctype is HTML 5 and it is different form HTML 4 in a way that HTML 5 has no DTD while the others have lots of including and excluding elements in DTD, so it is easier to start writing HTML file.

How is the markup for your document semantically correct? Are there any non-semantic elements that you needed for styling purposes? If you used divs and /or spans, be sure to account for them in your explanation.
-My HTML file is validated so it is semantically correct. I do not have any any non-semantic elements in a file. I use div to wrap around objective statement, education, work experience and skills because I want to make the topic and sub-topics to be one element. It is easier to style one element that topics and sub-topics separately.

In your own words, how is the hCard information you added useful to both humans and automated user agents trying to access your content?
-Each elements has its own class name so when humans read it, they would understand it right away. The automated user agent deals with hCard even better because it appears directly in codes that machines can understand.

References:
http://microformats.org/wiki/hcard
http://www.lipsum.com/
http://www.echoecho.com/htmllinks11.htm
http://www.w3schools.com/tags/tag_doctype.asp
http://www.w3schools.com/tags/tag_hr.asp
http://stackoverflow.com/questions/7096909/make-a-horizontal-rule-with-css-and-have-margins
http://jigsaw.w3.org/css-validator/#validate_by_upload
http://css-tricks.com/using-css-for-image-borders/
