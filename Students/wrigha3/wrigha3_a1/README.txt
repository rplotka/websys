Drew Wright

1: I used <!DOCTYPE html>, since I wanted to use HTML5 semantics,
and this is the only DocType for HTML5

2: As stated before, this is the only doctype for HTML5, this means
that it accepts everything in the HTML5 standard, and won't accept
anything that has been removed from HTML in HTML5, such as font tags.
HTML5 has removed a number of tags whose functionality is better
covered in CSS.

This also means that a number of tags are not supported as is, and
that I had to take a few steps to try an insure that the page
would at least function on older browsers.

3: Spans and Divs were used in the HCard portion of the Resume.
This was because semantic meaning is already being attached to
the info here through the HCard microformat, so I could use
spans and divs to get the data displayed on a browser more cleanly.
I also used a single <br/> tag in this portion

2: I have the entire Resume in an article tag, since this information
is standalone, and could easily be put into other locations.

The HCard is in a header, which is followed by sections for each
logical section of the Resume: Education, Work, and Skills. This
Follows the diagram for proper semantic use of header and section.

Within each section I have a header, which contains the name for the
section, and another section, which holds the actual content.

Both Education, and Work follow similar semantic organization.
I contain Schools and Companies in a list. Each of those 
elements then contains a list which holds information about
that institution. Lastly, each of these lists has an achievements
list which covers the achievements done at those institutions.

This listing was done both for style and semantic purposes. 
Stylistically, it made easy nesting. Semantically, it made
sense to nest info about the institution in a list.

3: The HCard is useful for humans, not because it makes the data
any more readable, but because browsers and add-ons
can be designed to take advantage of this mark-up to create features
like click to call, or click to add to contacts.

As for automatic agents viewing my content, the HCard format makes detailed
data about me readable. Without HCard, the closest tags are ones like address,
which is a catch-all for contact information, but doesn't associate that contact
info with any other data.
Using HCard those automatic agents could then store this resume in a database 
associated with me, or, like the users above send me calls, or emails(yay spam).

-------------------------------------------------------------------------------------------------------------
Websites Referenced


HTML5, CSS validated at: http://validator.w3.org/check
HCard validated at http://hcard.geekhood.net/

External Code Used/Referenced:
  Shiv
  Website: https://code.google.com/p/html5shiv/
  Purpose: Lets older versions of IE properly use CSS with new HTML5 semantic blocks
  
  HCard Creator
  Website: http://microformats.org/wiki/hcard
  Purpose: Referenced a standardized way through which HCards are made so that
            my HCard may be more easily read by machines.
  
Resources:
  Paper image taken from: http://s390.photobucket.com/user/plasticmadness/media/General%20Unsorted/background_patterns/paper.jpg.html
  Background taken from: http://nouseforaname.deviantart.com/art/Computer-Tile-11220937
References            
  http://htmlandcssbook.com/extras/introduction-to-hcard/
  Purpose: Learn about HCard microformat
  
  http://www.w3schools.com/
  Purpose: Learn everything about HTML/CSS

