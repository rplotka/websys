Name: Jed Kundl

1. I selected the doctype I'm using because it's what we were using in class, and it validated with me having to make the least amount of corrections.

2. I'm able to use newer tags and features that other/older doctypes wouldn't understand, and I'm unable to use depreciated tags that may have been available in older doctypes.

3. My HTML is semantically correct by using <i> and <b> tags instead of <em> or <strong> tags. Additionally, I use verbose class names to better define their purpose. I believe I explained the use of divs in the code. Primarily they were used when I wanted to float objects, however they were used in a couple of places because <p></p> tags weren't validating properly.

4. I actually think that the hCard information is less useful to the human reading it, because there is a lot of structure that needs to go into it, while most Humans have an understanding of addresses, and do not require tags to figure out precisely what's what. It's more useful to automated agents because is tags the information in a manner that is consistent with how other tag personal information. Meaning one automated script could pull massive amounts of information without worrying about formatting.

I primarily used W3Schools, StackOverflow, and Wikipedia as references for this assignment. I also asked questions of the instructor, friends in the class, friends outside the class, and people I didn't know that were sitting next to me while I was working.

I read about hCards here - http://microformats.org/wiki/hcard

While I did not intentionally go for the extra credit, I believe I met the requirements in designing my resume, as I wanted to make it look good from the start. I have a minimalistic approach to design. I made clean, clear cut sections, used rounded corners on my photo, arranged contact information in an easy to read, minimalistic manner, and used a sans serif font (Helvetica) for headers. I did not used a sans serif font on the actual information because at it's current size, it would make it harder to read on the web.

