Megan Matiz

Why did you select the doctype you used?

	I used HTML 4.01 Strict because it works well in all current browsers and 
	I did not need to use much presentation/styling for a resume.

How does your doctype compare to other commonly seen (strict) doctypes?

	HTML 4.01 Transitional is for pages with more presentational markup.
	XHTML is not used online often.

How is the markup for your document semantically correct? Are there any non-semantic elements
that you needed for styling purposes?If you used divs and /or spans, be sure to account for them 
in your explanation.
	
	I used divs and spans for the hcard because that's what everyone did for it
	when I was researching it. I used headings for headings and paragraphs for descriptions.
	I skipped h2 because I thought h3 was a better size.
 
In your own words, how is the hCard information you added useful to both humans and automated
user agents trying to access your content?
	
	The microformat makes the data more portable. For instance, there are browser extensions that
	can export hcards from sites to contact utilities. It's more portable because the tags all have
	specific class names. People can use the hCard like a business card.
	
 
A list of any and all works referenced throughout the assignment, including tutorials, reference
sheets, etc. Failure to cite all referenced works may result in a zero for the assignment and a
two-letter drop in your final grade. When in doubt, email the instructor or TA.

	http://en.wikipedia.org/wiki/Microformat
	http://microformats.org/wiki/hcard-authoring
	http://www.smashingapps.com/2010/02/24/why-you-should-have-a-vcard-and-inspiring-examples-of-personal-vcards.html
	http://microformats.org/wiki/hcard
	http://dev.opera.com/articles/view/introduction-to-hcard/
	http://dev.opera.com/articles/view/introduction-to-hcard-part-two-styling/
	http://www.w3schools.com/cssref/pr_background-position.asp
	http://dev.opera.com/articles/view/14-choosing-the-right-doctype-for-your/
	http://www.techrepublic.com/blog/web-designer/quick-tip-selecting-the-right-doctype/