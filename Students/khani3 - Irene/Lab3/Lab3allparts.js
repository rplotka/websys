//Recursively iterate through elements
root = document.getElementsByTagName("html")[0];

$( document ).ready(function()  { 
	//Add quote to end
	var last = document.getElementsByTagName("html")[0].lastChild;
	var quote = last.cloneNode(true); 
	quote.innerHTML = "\"Those who mind don't matter. Those who matter don't mind.\"";
	last.appendChild(quote);
	
	str = domIterate(root); 
	//Replace the element info with the string we are creating now
	document.getElementById('info').innerHTML = str;} );


function domIterate(current, depth) {
 //Set if depth not set already
 if (!depth)
 {
  depth = 0;
  }
  //If this is an element node, the constant is 1
  current.onclick = function() { alert(current.tagName); }
  current.onmouseover = function(event) { current.style.backgroundColor = "green"; } 
  current.onmouseout = function(event) { current.style.backgroundColor = "white"; } 
  //$(this).bind('click', function(event){ (current.style.backgroundColor = "black"; })
  //$(current).bind('mouseover', function(event){ (current.style.backgroundColor = "#ccc"; })
  if(current.nodeType == 1) {
   //the tree
   var txt = '';
   
   //One dash per point of depth
   var a;
   for(a= 0; a < depth; a++)
   {
    txt += '_';
  }
  //Add the element 's tag name to the end, after indicating depth
  txt += current.tagName + "\n";

  //For each child node
  var n;
  for(n = 0; n < current.childNodes.length; n++)
  {
   //Get text of child node (get its children)
   childTxt = domIterate(current.childNodes[n], depth+1);
   
   //If child can't be represented as text, add it to this one (full depth reached on that path)
   if(childTxt != null && childTxt != '')
   {
    txt += childTxt;
   }
  }
  return txt;
  }
  //Otherwise do nothing
  else
  {
   return null;
  }
}