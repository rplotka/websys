README.txt
Irene Khan
Web Sys Assignment 1

I selected HTML 4.01 Strict because I want to get used to using a standard 
syntax where I won't have the leeway of mixing up syntax from different versions, Hopefully, that way, I'll 
find it easier to catch my own mistakes, in the long run, before validating to find my inconsistensies.

I think one of the differences from other doctypes that I got caught on in the
validation is the meta charset = 'UTF-8' part. I thought it was just meta charset =
'UTF-8', but apparently for HTML 4.01 Strict, I need to write this:
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

Normally, I honestly throw divs around all over the place. But I tried to do this
without divs! I used headers for my name and for the categories, and lists and paragraphs
for all of the contained information. I initially had no divs, but then to use
microformatting, I added an outer div so I could wrap everything in a class "vcard". 
And I used spans in cases where I needed more outer layers for classes like "vcalendar"
and "education vevent", and also in places where I wanted different classes for pieces of the
same sentence or phrase (Ex. splitting the phrase "Ecofashion SF Web Developer" into an "organization"
class and a "position" class).

I think the hcard information is useful for transferring data. If I, or an automated
agent wanted to save someone's contact info somewhere, like into a user database or
into a phone, then that could be done since the data is conveniently formatted in a
standard way.

References:
I looked up the reference for the doctype. And I looked up how to use hcard
to tag resume info.
http://www.w3.org/TR/html4/sgml/dtd.html
http://microformats.org/wiki/hcard
http://microformats.org/wiki/faq
http://microformats.org/wiki/hresume
http://validator.w3.org/check
http://waterpigs.co.uk/php-mf2/?