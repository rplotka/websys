Chris Paradis

The main reason I chose XHTML to complete this assignment is because it has become what I 
am most comfortable with. While XHTML is more strict and requires more attention to detail
than HTML, I think it is easier to learn because of this. As a beginning web programmer, I can look
at my code and see that it is clean and very easy to follow. I will say it became somewhat of a nuisance at
times, especially when I had to constantly open and close <ul>s at one point in the code. Maybe I will 
venture out in later projects and try a different doctype when I feel more comfortable. 

XHTML is more strict than HTML 4.01 and 5. This makes the code easier to read and more predictable. 
I believe this also means it is eaier to learn. It is hard to be sloppy when the language I am most 
comfortable in prohibits sloppy code. Also the large amounts of validation tools for XHTML made it easier 
to get correctly than HTML might have been, although there were probably more changes to be made as a result. 

My markup is semantically correct because the page content must follows a logical structure, starts with the
<!DOCTYPE> declaration, my lists do not include block items such as paragraphs or tables, and tables were not
used in the layout. I did use divs and spans in the hCard section. I did so because in the research I did all
the examples used div and span, so I am assuming this has something to do with the construct of the "vcard" class
when the computer tries to read it. Otherwise I believe the code is semantically correct. 

The hCard information I added makes it possible for a machine to take that information in the hCard section
and store it in the correct location. It could make life easier for people in a number of ways. On one hand, 
someone reading my resume could store all of my contact information in the click of a button by having the 
machine fill it out for them. It could help me out as. When I applied for an IT analyst position at Cisco Systems
earlier this week I did not have to fill in any of the forms for them because their system could take it from my resume.
All in all, it is a time saver for everyone involved.

Sources

http://www.echoecho.com/htmllinks11.htm - code for email link

http://microformats.org/wiki/hcard - used the chart as a guide to construct my hCard code and learned hCard info

http://jigsaw.w3.org/css-validator/ - CSS validator

http://validator.w3.org/ - XHTML validation

http://www.w3schools.com/ - Tutorials

