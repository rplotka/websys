## Web Systems Development
### Assignment 1
Michael Agnew

agnewm@rpi.edu

==========================================

<b>Why did you select the doctype you used?</b>

I selected HTML5 because it would allow for the most different uses, and the newest features, but they can also work with all of the other doctype needs.

<b>How does your doctype differ from other commonly seen doctypes?</b>


HTML5 basically covers all of the capabuilities that 4.01 has plus some others that would have to be specified, such as svg and MathML.

<b>How is the markup for your document semantically correct?</b>

The document is setup semantically to cover all of the sections of information, and is named appropriately. The only other type of sections included are a dynamic sets of spaces for the sections of display. If you view the document in 3D mode in Mozilla Firefox, which has very good web development examination tools.

<b>How is the hCard information useful?</b>

It is useful because people can simply pull for data on a specific person, me, and add it to their list of people they want to email their offers out to. The downside is spam-bots could get me.

I referenced the W3 schools a lot for information regarding doctypes, and the hCard's microformat website.

I used google fonts for my web-fonts, which I believe gave my page a very sleek and modern look to it.


Sources:

Web-Fonts:
http://www.google.com/fonts/specimen/Rokkitt

W3 Schools:
http://www.w3schools.com/tags/tag_doctype.asp

Microformats:
http://microformats.org/code/hcard/creator