function htmlTree(obj, count){
	var obj = obj || document.getElementsByTagName('html')[0];

	var str = obj.nodeName + '\n';
	var element = document.getElementsByTagName(obj.nodeName);


	if(obj.hasChildNodes()){
		count += 1; //keeps track of the level of the current node in the tree, denoting how many '-'s to put in.
		var child = obj.firstChild;
		while(child){
			if(child.nodeType === 1){ //Checks if a child node exists
				for(var i = 0; i < count; i++)
					str += '-'; //append a dash
				str += htmlTree(child, count); //recurse
			}
			child = child.nextSibling;
		}
	}
	count -= 1;

	return str;
}


function mouseinFunc(){
	this.style.backgroundColor = "green";
	this.style.position = 'relative';
	this.style.right = '10px';
	
}

function mouseoutFunc(){
	this.style.backgroundColor = "white";
	this.style.position = 'relative';
}



var count = 0;
var obj;
document.getElementById('info').innerHTML =  htmlTree(obj, count); //recurse through the html tree

//add an onclick event listener to every element within the body, used query selector because getElementsByTagName was causing issues for this method.
document.querySelector('body').addEventListener('click', function(event) {
    alert(event.target.tagName);
  });



//run once the page is loaded.
window.onload = function(){


	var node = document.getElementById('favorite');
	var classes = node.className;
	//check if the "favorite" is a class.
	if(classes === 'quote'){
		var el = node.cloneNode(true);
		document.body.appendChild(el);
	}



	var divs = document.getElementsByTagName("div");

	for(var i = 0; i < divs.length; i++){

		divs[i].onmouseover = mouseinFunc;
		divs[i].onmouseout = mouseoutFunc;
	}
	
}