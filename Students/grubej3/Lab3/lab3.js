/*
Web Sys
Lab 4
Jonah Gruber
*/

root = document.getElementsByTagName('html')[0];
var str = domIterate2(root);

document.getElementById('info').innerHTML = str;
var quote = "\"If you wish to make an apple pie from scratch you must first invent the universe.\" -Carl Sagen";
addQuote(quote);
divFindAndColor();

//Function for part 1 of the lab: Iterating throught the dom
function domIterate(current, depth) {
	if (!depth) {
		depth = 0;
	}

	if (current.nodeType == 1) {
		var txt = '';

		for (var i = 0; i < depth; i++) {
			txt += '-'
		}
		txt += current.tagName + "\n";
		for(var n = 0; n < current.childNodes.length; n++) {
			childTxt = domIterate(current.childNodes[n], depth+1);
			if(childTxt != null && childTxt != '') {
				txt += childTxt;
			}
		}
		return txt;
	} else {
		return null;
	}
}

//Function for part 2 of the lab, similar to part 1 except now the function add an onclick action
function domIterate2(current, depth) {
	if (!depth) {
		depth = 0;
	}

	if (current.nodeType == 1) {
		var txt = '';

		current.onclick=function(){alert(current.tagName);};


		for (var i = 0; i < depth; i++) {
			txt += '-'
		}

		txt += current.tagName + "\n";



		for(var n = 0; n < current.childNodes.length; n++) {
			childTxt = domIterate2(current.childNodes[n], depth+1);
			if(childTxt != null && childTxt != '') {
				txt += childTxt;
			}
		}
		return txt;
	} else {
		return null;
	}
}

// Function for part 3 of the lab
// copies a quote and replaced the html with quotes prameter
function addQuote(quote){
	
	var quotes = document.getElementsByClassName('quote');
	var newQuote = quotes[0].cloneNode();
	newQuote.innerHTML = quote;
	document.getElementsByTagName('body')[0].appendChild(newQuote);
}

//Second function fo rpart 3 of the lab
//Function adds CSS styling to moused over divs
function divFindAndColor(){
	var divs = document.getElementsByTagName('div');

	for(var i = 0; i < divs.length; i++) {
		divs[i].onmouseover =function() {
				this.style.background = "red";
				this.style.paddingLeft = "10px";
		}
		divs[i].onmouseout =function() {
				this.style.background = "white";
				this.style.paddingLeft = "0px";
		}
	}
}
