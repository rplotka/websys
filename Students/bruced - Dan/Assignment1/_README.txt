README: bruced, assignment 1

I chose XHTML Strict, although I am not 100% pleased with the results. The code does not validate fully because I include divs inside of anchors, but I did not know of another way to make an entire container a hyperlink. Also, "target" is not listed as an attribute in strict, only in transitional, and I used this--which is supported by all major browsers--to force links to open in a new tab.

XHTML Strict is a very standard doctype, because I did not want to play with HTML5. I am not completely sure of the comparison between those two, but XHTML is symantic, whereas vanilla HTML is not.

All of my elements are within classed or IDed containers. I had to fiddle here and there to make everything look how I wanted. An example of this is my image. I did not want it pushed over when a browser window was small, so I used an absolute position.

hCard is useful to humans and computers because:
To humans, it provides a simple, readable format.
To computers: it encases everything in classes that robots and other automated scripts can find easily, read the information of, and act on.

I refereced various websites for both anchor tag issues and hCard issues, including StackOverflow and Microformats.org. I also researched different doctypes using Wikipedia and W3C as bases (read: plural of "basis").