My Name: Mike Xie
HTML5 is the latest standard for HTML. Many major browsers support it. Flash is made unnecessary on HTML5 documents. Plus The DOCTYPE declaration is very simple.
HTML5 includes new elements for drawing graphics, adding media content, better page stucture, etc. There are also HTML4 elements deprecated in HTML5. 
HTML5 does not require all tag names to be lowercase and closing empty elements as in XHTML.
HTML5 uses simpler syntax to specify DocType, character encoding, script, and link tag than HTML4.
I use html table elements extensively in constructing the document because that way it is easier to manage layout.
Firstly, the hCard information is human readable.
Secondly, automated user agents such as programs can import, index, and remix as native data given the semantic information provided by hCard microformat.
Thirdly, you can publish your contact info in a more meaningful way understood both by human and machines as XHtml 

http://www.w3schools.com/html/html5_new_elements.asp
http://www.tutorialspoint.com/html5/html5_syntax.htm 