root = document.getElementsByTagName('html')[0];
var str = domIterate(root);
document.getElementById('info').innerHTML = str;

var div = document.getElementsByTagName('div')[1];
var newClone = div.cloneNode(true);
document.body.appendChild(newClone);

var para = document.getElementsByTagName('div');
for(var i = 0; i < para.length; i++) {
  para[i].onmouseover = function(e) {
  	this.style.position = "relative";
    this.style.backgroundColor = "#234500";
    this.style.right = "10px"; 
  }
  para[i].onmouseout = function(e) {
  	this.style.position = "static";
    this.style.backgroundColor = "#fff";
  }
}


function domIterate(current, depth){
	if (!depth){
		depth = 0;
	}
	if (current.nodeType == 1){
		var txt = '';
	
		for (var i = 0; i < depth; i++) {
			txt += "-";
		} 
		txt += current.tagName + "\n";
		current.onclick=function(e){
			alert("This is the "+current.tagName); 
		};
		for (var n = 0; n <current.childNodes.length; n++) {
			childTxt = domIterate(current.childNodes[n], depth+1);

			if (childTxt != null && childTxt != '') {
				txt += childTxt;
			}
		}

		return txt;
	}
	else {
		return null;
	}
	
}
