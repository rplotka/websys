Name: Lauren Angelini

1. Why did you select the doctype you used?:
I picked HTML because it is the doctype I'm most comfortable working with and
had all the attributes I needed for the assigment. I also believe it's 
more user friendly than the other doctypes.

2. How does your doctype compare to other commonly seen (strict) doctypes?
HTML5 headings like <header>, <footer>, <nav> etc makes code simpler and easier
to read. Another user can easily look at the code and tell what corresponds to what
part of the webpage. Most popular browsers support HTML5 (Chrome, Safari, Firefox etc.), it
was created with this in mind.  


3. How is the markup for your document semantically correct? Are there any non-semantic 
elements that you needed for styling purposes? If you used divs and /or spans, be 
sure to account for them in your explanation.
I used one div contained to fix the width, and center my content. I added a 
border and some padding in this section as well. No other container would have worked
as well or made logical sense in this scenario. Other than that, my document is
semantically correct. It's also good if there is confusion with contact information. 
For example, different addresses or telephone numbers. 


4. In your own words, how is the hCard information you added useful to both humans and 
automated user agents trying to access your content?
It's useful for quick access to my contact information without having to search 
through my resume or longer documents.  


5. References:
http://www.w3schools.com/
http://microformats.org/wiki/hcard

