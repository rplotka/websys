
var ret_string = ""

function iter_dom(node, dashes) {

    if (typeof node.tagName !== 'undefined') {
        ret_string += dashes + node.tagName + "<br>";
    }

    for (var i = 0; i < node.childNodes.length; i++) 
    {
        iter_dom(node.childNodes[i], dashes + "-");
    }

    return ret_string;
}

function clickResponse() {
    window.alert(this.tagName);
}

function mouseOver() {
    this.style.background = "yellow";
    this.style["padding-left"] = "10px";
}

function mouseLeave() {
    this.style.background = "white";
    this.style["padding-left"] = "0px";
}


function init() 
{
    var body = document.body;
    var body_elems = body.childNodes;

    var once = false;

    //Get for each child element in the body:
    for (var i = 0; i < body_elems.length; i++) {
        body_elems[i].onclick = clickResponse;
        body_elems[i].onmouseover = mouseOver;
        body_elems[i].onmouseout = mouseLeave;

        if (i === body_elems.length - 2 && !once) {

            var new_node = body_elems[i].cloneNode(true);
            new_node.innerHTML = "The best defense is a good offense";
            body.appendChild(new_node);
            once = true;
        }

    }

    var html = document.getElementsByTagName('html')[0];
    var str = iter_dom(html, "");

    document.getElementById("info").innerHTML = str;

}

window.onload = init;