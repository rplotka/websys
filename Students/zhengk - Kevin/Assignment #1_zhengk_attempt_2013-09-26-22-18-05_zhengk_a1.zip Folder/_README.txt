Kevin Zheng

Q. Why did you select the doctype you used?
A. I used HTML 4.01 Strict, as this is good for reinforcing best-practices to a novice HTML writer like me. I want to make sure that the document conforms to the most standard, accepted HTML practices possible.

Q. How does your doctype compare to other commonly seen (strict) doctypes?
A. HTML 4.01 Strict does not allow depreciated elements or framesets, compared to HTML 4.01 Transitional and HTML 4.01 Frameset. It's generally standard, unlike HTML5, which is not yet so.

Q. How is the markup for you document semantically correct? Are there any non-semantic elements that you needed for styling purposes? If you used divs and /or spans, be sure to account for them in your explanation.
A. The markup is semantically correct because I use strong tags for important items such as section titles and organizations. I use paragraph tags whenever I need to start a new section or entry, instead of divs. I use unordered lists whenever I need to list items. The only time I use <span> is in the contact info, where I need to carve out the specific text to tag with hCard information, but the hCard tagging makes up for it by providing semantic information in the document about my contact info. This is also the only time I use <div>, where I need it to create a block representing the hCard info that holds only classes that belong to hCard.

Q. In your own words, how is the hCard information you added useful to both humans and automated user agents trying to access your content?
A. It allows humans trying to search my name on a search engine to find attributes related to my name. It allows automated agents like search engines and web crawlers to better associate my page to its meaning (semantics) so as to improve the relevance of the page when people search my name or attributes (education, skills, etc).

Referenced works:

http://en.wikipedia.org/wiki/Microformat
http://en.wikipedia.org/wiki/HCardhttp://microformats.org/wiki/hcard
http://microformats.org/wiki/hcard
http://www.w3schools.com/css/default.asp
Class Presentation Powerpoint: Markup Languages
Class Presentation Powerpoint: Cascading Style Sheets (CSS)
Class Presentation Powerpoint: CSS - Beyond the Basics