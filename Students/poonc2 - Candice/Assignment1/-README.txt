Name: Candice Poon
-I selected XHTML 1.0 strict because it was the default selected when typing in the doctype snippet here on Sublime Text. 
-I designed my resume to be semantically correct in a few different ways. I first made sure all of my classes accurately described what exactly it was on my resume. Whether it was the category name, the job description, or the job title. I also added hCard information which will make it easy for machines to parse my HTML code and find useful information such as my address, phone, and email. 

Works reference:
www.w3schools.com
YOU (as in Richard Plotka) <-Thanks for all the help!