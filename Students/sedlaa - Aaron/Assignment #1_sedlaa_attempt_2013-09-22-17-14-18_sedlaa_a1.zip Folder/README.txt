Name: Aaron Woodrow Sedlacek, 9/22/2013

Markup and CSS validate with W3C's validators. CSS validates as CSS level 3.

Why did you select the doctype you used?

I selected this doctype because it seemed to be the most straightforward and easy doctype for my limited experience.
Also this doctype is supported in most browsers. XHTML 1.0 strict has been a W3C Recommendation since 2000.

Why did you select the doctype you used?

This doctype is one of the more prevalently used doctypes accross the web.

How is the markup for your document semantically correct? Are there any non-semantic elements that you needed for styling purposes? If you used divs and /or spans, be sure to account for them in your explanation.

Indentation is done correctly in order to reflect the nesting of the document. The only non semantic element that I used was <div>. I used div's in the contact information in order to do the hcard stuff for my address. I probably shouldn't have, but it seemed like the easiest way to incorporate the street-address, locality, region, and postal-code classes.

In your own words, how is the hCard information you added useful to both humans and automated user agents trying to access your content?

the hcard information makes it easier for both human and automated users to locate important information. In my opinion, I think that it aids to simplify locating certain resources for content viewers.

References:

http://microformats.org/wiki/hcard
http://www.w3schools.com/css/
http://www.w3schools.com/html/