//Lab 3 - James Arey
//Parts I and II done here along with aspects of part III
//The beginning of the only html document in the directory
root = document.getElementsByTagName('html') [0];
//Calls the function used to acquire the text to be put in the "info" element
var str = domIterate(root);
//Enter the text into the "info" element
document.getElementById('info').innerHTML = str;


//Some of Part III done here
//Gets a variable to hold the html document
clone = document.getElementsByTagName('html') [0];
//Sets clone to be the body node and then the first DIV node clones it and adds it to the end of body
clone = clone.childNodes[2];
daclone = clone.childNodes[5].cloneNode(true);
document.getElementsByTagName('body') [0].appendChild(daclone);
//Acquires the newly appended node 
clone = clone.childNodes[14];
//Sets that node to move to the right 10px and turn red on mouse over
clone.onmouseover=function()
{
	clone.style.backgroundColor = "red";
	clone.style.paddingLeft = "10px";
};
//Sets the node to requtrn to normal style on mouse out
clone.onmouseout=function()
{
	clone.style.backgroundColor = "white";
	clone.style.paddingLeft = "0px";
};


//Function to iterate the tags of the html document setting up Parts I and II, along with aspects of Part III
function domIterate(current, depth) 
{
	//If at the beggining of the document the depth is 0
	if (!depth)
	{
		depth = 0;
	}

	//Part I - III
	//Does the necessary work of the node if the node is an ELEMENT_NODE (1)
	if(current.nodeType == 1)
	{

		//Part II
		//Adds on click listener to display each tag name in ascending order of the location clicked
		current.onclick = function(e)
		{
			alert(this.tagName);

		};



		//Part III
		//Adds the onmouseover and out listeners to the DIVs 
		if (current.tagName == 'DIV')
		{
			current.onmouseover=function()
			{
				current.style.backgroundColor = "red";
				current.style.paddingLeft = "10px";
			};
			current.onmouseout=function()	
			{
				current.style.backgroundColor = "white";
				current.style.paddingLeft = "0px";
			};
		}


		//Part I
		//Creates txt variable to add the appropriate number of "-"s to the beggining of the tag name
		var txt = '';
		//Add the number of "-"s to the beggining of the tag equal to its depth
		for(var i = 0; i < depth; i++)
		{
			txt += '-';
		}
		txt += current.tagName + "\n";


		//Iterates the current Node's children to recursively call this function on, 
		//thus creating a list of Nodes
		for (var n = 0; n < current.childNodes.length; n++)
		{
			childTxt = domIterate(current.childNodes[n], depth+1);
			if(childTxt != null && childTxt != '')
			{
				txt += childTxt;
			}
		}
		//Returns the end result of the children nodes combined with the current nodes to the parent,
		//all the way back to the top html node
		return txt;
	}




	//If not an ELEMENT_NODE skip it
	else
	{
		return null;
	}

}

