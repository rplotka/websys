function htmlTree(depth, obj){  
	if(obj && obj.nodeType == 1) {
		var str = '';
		for (var i = 0; i < depth; i++) {
			str += '-';
		}
		str += obj.tagName + '<br/>';     
		
		obj.onclick = function(e) {
			alert(this.tagName); 
		};
		if(obj.tagName == "DIV") {
			obj.onmouseover = function(e) {
				this.style.paddingLeft = "10px";
				this.style.backgroundColor = "#ccc";
			};
			obj.onmouseout = function(e) {
				this.style.paddingLeft = "0px";
				this.style.backgroundColor = "#fff";
			};
		}
		
		for(var j = 0; j < obj.childNodes.length; j++) {
			childstr = htmlTree(depth + 1, obj.childNodes[j]);
			if(childstr != '' && childstr != null) {
				str += childstr;
			}
		}
		return str;
	} else {
		return null;
	}
}

document.getElementById('info').innerHTML += '<br/>';
var str = htmlTree(0, document.getElementsByTagName('html')[0]); 
document.getElementById('info').innerHTML += str;


var obj = document.getElementsByTagName('div')[3].cloneNode(true);
obj.onmouseover = function(e) {
	this.style.paddingLeft = "10px";
	this.style.backgroundColor = "#ccc";
};
obj.onmouseout = function(e) {
	this.style.paddingLeft = "0px";
	this.style.backgroundColor = "#fff";
};
document.body.appendChild(obj);