$(document).ready(function(e) {
	var i = 0;
	// alert("here");
	$.ajax({
		type: "GET",
		url: "lab5.json",
		dataType: "json",
		success: function(responseData, status) {
			var output = "";
			$.each(responseData.menuItem, function(i, item){
				output += '<li>';
				output += '<a href="#">' + item.menuName + '</a>';
				output += '<ul>';
				output += '<li>' + '<a href="' + item.menuURL + '">';
				output += item.menuDesc + '</a>';
				output += '</li>';
				output += "</ul>";
				output += "</li>";
			});
			$('#menu').html(output).menu("refresh");

		}, error: function(msg) {
			alert("There was a problem: " + msg.status + " " + msg.statusText);
		}		
	});
});

  $(function() {
    $( "#menu" ).menu();
  });
