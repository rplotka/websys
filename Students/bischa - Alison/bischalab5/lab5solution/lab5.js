

$(document).ready(function() {
  $.ajax({
    type: "GET",
    url: "menuItems.xml",
    dataType: "xml",
    success: function(responseData, status) {
      var menuli = '';
      $(responseData).find("menuItem").each(function() {
         menuli += '<li>';
         menuli += '<a href="#">' + $(this).find("menuName").text() + '</a>';
         menuli += '<ul>';
         menuli += '<li>' + '<a href="' + $(this).find("menuURL").text();
         if ($(this).find("menuName").text() == 'Lab 5') {
          menuli += '" id="swx" onmouseover="switchText();" onmouseout="switchText();';
         }
         menuli += '">' + $(this).find("menuDesc").text() + '</a></li>';
         menuli += '</ul>';
         menuli += '</li>';

         $("#accordion").html(menuli).accordion("refresh");
         $("#menu").html(menuli).menu("refresh");
      })
    }, error: function(msg) {
      alert("There was a problem: " + msg.status + " " + msg.statusText);
    }
  });
});

$(function() {
  $( "#accordion" ).accordion();
});

$(function() {
  $("#menu").menu();
});

function switchText() {
  var id = $("[id^=swx").attr("id");

  if(id=="swx") {
    $("#swx").text("Quiz 1").removeAttr("id").attr("id", "swxback");
  } else {
    $("#swxback").text("Lab 5").removeAttr("id").attr("id", "swx");
  }
}


