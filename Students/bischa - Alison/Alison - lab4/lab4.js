$('#coverart').click(function(e) {

	$.ajax({
		type: "GET",
		url: "lab4.json",
		dataType: "json",
		success: function(responseData, status) {

			var title = '';
			var album = '';
			var artist = '';
			var date = '';
			var genre = '';

			$.each(responseData.song, function(i, item) {

			// $.each(responseData.song, function(i, item) {
				title += '<li>' + item.Name + "</li>";
				artist += '<li>' + item.Artist + "</li>";
				album += '<li>' + item.Album + "</li>";
				date += '<li>' + item.YearReleased + "</li>";
				genre += '<li>' + item.Genre + "</li>";

				if(i==0){
					$("a").last().attr("href", item.Website).text(item.Website);
					$("img").last().attr("src",item.AlbumImage);
				}
				else {
					$(".siteli").last().clone().appendTo("#site");
					$("a").last().attr("href", item.Website).text(item.Website);
					$(".coverli").last().clone().appendTo("#coverart");
					$("img").last().attr("src", item.AlbumImage);
				}
			// )};
				});

			$("#title").html(title);
			$("#artist").html(artist);
			$("#album").html(album);
			$("#date").html(date);
			$("#genre").html(genre);
			$(".footer").html('<li>End of List</li>');
		}, error: function(msg) {
			alert("There was a problem: " + msg.status + " " + msg.statusText);
		}		
	});
});