Alison Bischak


Why did you select the doctype you used?
I used the XHTML 1.0 Strict doctype, because this was the one already at the top of my file when I used it for the Intro to IT lab 2. I didn't chage it because it seemed like a good choice because it requires the markup to be written as well-formed XML.

How does your doctype compare to other commonly seen (strict) doctypes?
Compared to HTML 4.01 strict, my doctype seems about the same except for the well-formed XML part.

How is the markup for your document semantically correct? Are there any non-semantic elements that you needed for styling purposes? If you used divs and /or spans, be sure to account for them in your explanation.
I used divs in order to break up the separate categories of my resume for easier styling. I am not aware of any non-semantic elements that I used for styling purposes. 

In your own words, how is the hCard information you added useful to both humans and automated user agents trying to access your content?
Standardizing the class names for each piece of information makes it easier to identify and style these objects without searching through the code for them. 

Sources:
http://en.wikipedia.org/wiki/HCard
www.w3schools.com
http://www.w3schools.com/tags/tag_doctype.asp
http://www.w3schools.com/cssref/css3_pr_transform.asp
http://stackoverflow.com/questions/10517020/how-to-import-local-image-using-knitr-for-markdown