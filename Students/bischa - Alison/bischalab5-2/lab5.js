$(document).ready(function(e) {
	var i = 0;
	// alert("here");
	$.ajax({
		type: "GET",
		url: "lab5.xml",
		dataType: "xml",
		success: function(responseData, status) {
			var output = '';
			$(responseData).find("menuItem").each(function() {
				output += '<li>';
				output += '<a href="#">' + $(this).find("menuName").text() + '</a>';
				output += '<ul>';
				output += '<li>' + '<a href="' + $(this).find("menuURL").text() + '">';
				output += $(this).find("menuDesc").text() + '</a>';
				output += '</li>';
				output += "</ul>";
				output += "</li>";
			});
			$('#menu').html(output).menu("refresh");

		}, error: function(msg) {
			alert("There was a problem: " + msg.status + " " + msg.statusText);
		}		
	});
});

$(function() {
  $( "#menu" ).menu();
});
