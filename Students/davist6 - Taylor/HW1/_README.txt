Taylor Davis
-I chose HTML5 because it is what we've used in labs and in class, and is therefore what I am most familiar with.
-HTML5 has has defined more commonly used tags than previous doctypes, allowing for more standardized and easily read output.
-I created a few non-semantic elements such as <name>, <sect>, and <pretty> so I could manipulate certain elements (for stylistic reasons) using CSS. I also used a div and
span in the name of form. To center the text in the browser (while still allowing a left hand alignment) I used a div, and a span was needed to round the 
corners of my photo.
-As hCard information is standardized it allows a computer to easily search for specific information within a document. Additionally given that there are some culturally 
standard ways of formatting information (such as an address) hCard can automatically format the data into the expected format, rather than force the coder to include
additional information everytime.
References:
http://www.wikihow.com/Center-Web-Page-Content-Using-CSS
http://en.wikipedia.org/wiki/HCard
http://sillygrrl.com/2013/05/15/how-to-create-circle-photos-rounded-corners-with-css/
http://microformats.org/wiki/hcard#Example
http://www.w3schools.com/html/html_images.asp
http://www.w3schools.com/tags/tag_doctype.asp