
var root = document.getElementsByTagName('html')[0];

var str = domIterate(root);
document.getElementById('info').innerHTML = str;

function domIterate(current, depth) {
    if (!depth) {  //set the depth initially at 0
        depth = 0;
        
    }
    if (current.nodeType == 1) {
        var txt = '';
        
        for (var i= 0; i < depth; i++) {
            txt += '-';
            
        }
        txt += current.tagName + "\n";

        //part 2
        current.onclick = function(e) {
          alert(this.tagName);
        };
       
        
        for (var n = 0; n < current.childNodes.length; n++) {
            var childTxt = domIterate(current.childNodes[n], depth+1);
            if (childTxt != null && childTxt != '') {
                txt += childTxt;
            }
        }
        return txt;
    }
    else {
        return null;
    }
}
//first part of section 3
function copy(){
    var t = document.getElementsByClassName('quote')[0].cloneNode(true);
    document.body.appendChild(t);
}
copy();
//second part of 3
//when the mouse hovers over a div, certain things change
function over(elem) {
    elem.style.backgroundColor = "#C0C0C0";
    elem.style.paddingLeft = "10px";
}
function exit(elem) {
    elem.style.backgroundColor = "white";
    elem.style.paddingLeft = "0";
}