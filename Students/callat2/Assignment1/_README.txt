T.J. Callahan
Homework 1 readme
9/26/2013

Doctype:
    I selected the HTML 4.01 strict doctype because I have only done very basic web development and I wanted to keep it simple and use the DTD with which I was most familiar. Since this assignment is relatively simple, it wasn't so restrictive that I had to consider using a different doctype.

Comparison to other doctypes:
    HTML 4.01 excludes the now obsolete styling and presentation attributes since stylesheets are now widely used. XHTML 1.0 is based on XML and is stricter, since it requires the markup to be well-formed. XHTML essentially mirrors HTML using XML, and it is parsed by an XML parser as opposed to an HTML-specific parser. HTML5 is the new version of the HTML doctype that removes many obsolete elements (such as font) and adds some diversity in elements for better structure.

Markup:
    I did my best to make my markup as semantically clear as possible. I did this by using header tags for consistent purposes-- h1 was only used on my name at the top of the sheet, h2 was used for the section titles, h3 for school and workplace names, and h4 for my titles/educational programs I was enrolled in and for list titles. I used divs for styling purposes to make it easy for me to delineate the important sections of my resume. I made sure that the divs all had classes that described their purpose as part of the markup. The reason I created a "listbox" div within each "resumebox" div was to make it easier to manipulate the list styling independently of the container and title styling.

hCard:
    hCards are useful to humans because they appear on the webpage in an easily recongnizeable address layout. They are also useful to humans that are reading the code because it is a standard way to mark up your contact information and is easy to recognize in the html. It is useful to machines because it includes information that doesn't display on the page, but helps automated processes like search engines to find the page regardless.

Styling:
    I chose a relatively muted color palette to make the site more readable and put less strain on the eyes. This means less white and more light grey, few primary colors, and a homogenous aesthetic. To complement this, I chose to round the corners slightly on the boxes and add a slightly darker grey border. The effect of all of this is a streamlined, soft design that is pleasing to the eyes.


references:
hCard microformat from http://microformats.org/wiki/hcard-authoring

Resume styling loosely inspired by http://themeforest.net/item/clean-cv-resume-html-template-4-bonuses/full_screen_preview/82474?ref=NetPremium

Information on doctypes from http://www.w3.org/QA/2002/04/valid-dtd-list.html