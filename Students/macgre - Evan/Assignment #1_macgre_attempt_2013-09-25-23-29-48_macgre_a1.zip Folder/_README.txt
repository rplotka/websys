Evan MacGregor


o Why did you select the doctype you used?
It was one we had previously used in Intro to ITWS, and I knew that it allowed use of more elements than XHTML Strict that I was unsure of whether I would need.

o How does your doctype compare to other commonly seen (strict) doctypes?
It contains all HTML elements and attributes, and allows presentational and deprecated elements, but does not allow framesets.

o How is the markup for your document semantically correct? Are there any non-semantic elements that you needed for styling purposes? If you used divs and /or spans, be sure to account for them in your explanation.
It is organized in a way that is both machine- and human-readable. I made the styling work around the semantic markup, and used divs almost exclusively to describe elements outside of unordered lists or within list items.

o In your own words, how is the hCard information you added useful to both humans and automated user agents trying to access your content?
I was unsure of how to add in the hCard format, so I added markup that seemed semantically correct. hCard-tagged elements have additional styling, and link to their respective sites. It still seems as though it doesn't change much, however. I don't see how the information is any easier for humans and bots to access.


Works Referenced:
  My own resume
  http://www.w3schools.com/tags/tag_doctype.asp
  http://microformats.org/wiki/hcard-examples
  http://css.maxdesign.com.au/listutorial/horizontal_master.htm
  http://www.color-hex.com/
  Google Fonts - Open Sans