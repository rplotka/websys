/* PART 1 */
var root = document.getElementsByTagName("html")[0];
var result = domIterate(root,0);

document.getElementById("info").innerHTML = result;

function domIterate(current,depth){
  if (!depth) { depth = 0; }
  if (current.nodeType == 1) {
    var text = "";
    for (var i = 0; i < depth; i++) {
      text += "-";
    }
    text += current.tagName + "\n";
    for (var j = 0; j < current.childNodes.length; j++) {
      childText = domIterate(current.childNodes[j], depth+1);
      if (childText != null && childText != "") {
        text += childText;
      }
    }
    /* PART 2 */
    current.onclick = function(e) {
      alert(this.tagName);
    };
    return text;
  } else {
    return null;
  }
}

/* PART 3 */
var quotes = document.getElementsByClassName("quote").length;
var last_quote = document.getElementsByClassName("quote")[quotes-1];
var new_quote = last_quote.cloneNode(false);

var text_node = document.createTextNode("'The programmer, like the poet, \
 works only slightly removed from pure thought-stuff. He builds castles \
 in the air, from air, creating by exertion of the imagination. Few media \
 of creation are so flexible, so easy to polish and rework, so readily \
 capable of realizing grand conceptual structures. Yet the program \
 construct, unlike the poet's words, is real in the sense that it \
 moves and works, producing visible outputs separate from the construct \
 itself. It prints results, draws pictures, produces sounds, moves arms. \
 The magic of myth and legend has come true in our time. One types the \
 correct incantation on a keyboard, and a display screen comes to life, \
 showing things that never were nor could be. ... The computer resembles \
 the magic of legend in this respect, too. If one character, one pause, \
 of the incantation is not strictly in proper form, the magic doesn't \
 work. Human beings are not accustomed to being perfect, and few areas \
 of human activity demand it. Adjusting to the requirement for perfection \
 is, I think, the most difficult part of learning to program.' - F. \
 Brooks");
new_quote.appendChild(text_node);

var body = document.getElementsByTagName("body")[0];
body.appendChild(new_quote);

var divs = document.getElementsByTagName("div");

for (var k = 0; k < divs.length; k++) {
  divs[k].onmouseover = function(e) {
    this.style.background = "yellow";
    this.style.position = "relative";
    this.style.left = "10px";
  };
  divs[k].onmouseout = function(e) {
    this.style.background = "white";
    this.style.position = "";
    this.style.left = "0px";
  };
}