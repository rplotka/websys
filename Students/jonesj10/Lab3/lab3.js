var root = document.getElementsByTagName('html')[0];
var str = domIterate(root);
document.getElementById('info').innerHTML = str;

function domIterate(element) {
	return parse(element, 1);
}

// Parse an element and its children
function parse(element, depth) {
	// Check if element is an html element
	if (element.nodeType == 1) {
		// add dashes indicating depth and tagname
		var str = Array(depth).join('-') + element.tagName + "\n";
		
		// add onclick event to element
		element.onclick = function() {
			alert(element.tagName);

			// console is less annoying than alerts ...
			//console.debug(element.tagName);
		};

		// get children
		var children = element.childNodes;

		// parse children
		for (var i=0; i<children.length; i++) {
			var childStr = parse(children[i], depth+1)
			if (childStr != null && childStr != '') {
				str += childStr;
			}
		}
		return str;
	} else {
		return null;
	}
};

// clone quote div and add my favoite quote
var favQuote = document.getElementsByClassName('quote')[1].cloneNode(true);
favQuote.innerHTML = "Fly, you fools! --Gandalf"

// append my quote to the end of the body
var body = document.getElementsByTagName('body')[0];
body.appendChild(favQuote);

// Add mouse event listeners to all divs
var divs = document.getElementsByTagName('div');
for (var i=0; i<divs.length; i++) {
	d = divs[i];
	
	d.onmouseover = function() {
		this.style.background = "lightblue";
		this.style.paddingLeft = "10px";
	};

	d.onmouseout = function() {
		this.style.background = "white";
		this.style.paddingLeft = "0px";
	}

}
