
root = document.getElementsByTagName('html')[0];
var str = domIterate(root);

document.getElementById('info').innerHTML = str;


function domIterate(current, depth){
	//set depth if not already set
	if(!depth){
		depth = 0;
	}
	
	//if this is an element node (the constant is 1)...
	if(current.nodeType == 1){
		var txt = '';
		
		for(var i = 0; i < depth; i++){
		  txt += '-';
		}
		
		//Add the element's tag name to the end, after indicating depth
		txt += current.tagName +"\n";
		
		//Begin part 2
		//Bind the onclick handler here.
		//Not that this isn't the most efficient way of doing this...
		current.onclick = function(e){
		  alert(this.tagName);
		};
		
		//End part 2
		
		// For each child node
		for (var n = 0; n < current.childNodes.length;n++){
			childTxt = domIterate(current.childNodes[n], depth+1);
			
			// if the child node can be represented by text, add it to this one.
			if(childTxt != null && childTxt != ''){
			  txt += childTxt;
			}
		}
		return txt;
	}
	else{
	  return null;
	}
  }

var element = document.getElementsByClassName('quote')[1];
var copy = element.cloneNode(true);
document.body.appendChild(copy);
  
var selects  = document.getElementsByClassName('quote');  

  for(var i =0; i < selects.length; i++){
     selects[i].onmouseover = function(){
	 this.style.padding="0 0 0 10px";
	 this.style.background="#FFE";
	};
	 selects[i].onmouseout = function(){
	 this.style.padding="0 0 0 0";
	 this.style.background="white";
	 };
  }

