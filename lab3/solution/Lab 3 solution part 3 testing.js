    /**
     * Part 1
     */

    // Target the html element. There's only one, but getElementsByTagName()
    // will always return an array, so we want the first element.
    root = document.getElementsByTagName('html')[0];
  
    // Build up our text-based tree, and put it inside our #info element.
    var str = domIterate(root); 
    document.getElementById('info').innerHTML = str;

    // Recursively iterate through the DOM, starting with a given root element.
    // This function only iterates through Element nodes, so the base case is
    // the one in which no Element children exist (non-Element nodes will
    // return null instead of their tagName).

    function domIterate(current, depth) {
      // Set the depth if it wasn't set already.
      if (!depth) {
        depth = 0;  // No need for var; it's an argument.
      }
    
      // If this is an Element node (the constant is 1)...
      if (current.nodeType == 1) {
        // Start building the text of this node, pre-traversal.
        var txt = '';

        // One dash for each point of depth
        for (var i = 0; i < depth; i++) {
          txt += '-';
        }

        // Add the element's tag name to the end, after indicating depth
        txt += current.tagName + "\n";

        /**
         * Part 2
         */

        // Bind the onclick handler here.
        // Note that this isn't the most efficient way of doing this...
        current.onclick = function(e) {
          alert(this.tagName);
        };

        /**
         * End Part 2
         */

        // For each child node
        for (var n = 0; n < current.childNodes.length; n++) {
          // Get the text of the child node
          childTxt = domIterate(current.childNodes[n], depth+1);

          // If the child node can be represented by text, add it to this one.
          if (childTxt != null && childTxt != '') {
            txt += childTxt;
          }
        }
        // Return the text of this node and all children.
        return txt;
      }
      // Otherwise, do nothing.
      else {
        return null;
      }
    }

    /**
     * Part 3
     */

    // Retrieve all divs in the document as Element objects.
    var divs = document.getElementsByTagName('div');

    // I chose the first, so we'll copy it (and all children).
    // This is done using the clondeNode method of our chosen Element object.
    var favorite = divs[0].cloneNode(true);

    // We want to add this to the end of our collection of divs. 
    // There are many ways of doing this, including iterating through the DOM
    // as we have in Part 1. The easiest is to simply refer to the parentNode.
    divs[0].parentNode.appendChild(favorite);

    // Update our collection, so we apply handlers to the copy as well.
    divs = document.getElementsByTagName('div');
    // alert(divs.length);


// from Natalie L
    quotes = document.getElementsByClassName('quote');
    // alert(quotes.length);

   for(var i=0, i<selects.length;i++){
      selects[i].addEventListener('onmouseover',mOver,false);
  }

  function mOver(){
          this.style.padding="0 0 0 10px";
          this.style.background="#FFFAFA";
  }



    // Iterate through each of our divs.
    // for (var i = 0; i < divs.length; i++) {
    //   // Assign our event handlers.
    //   divs[i].onmouseover = function(e) {
    //     // Change the background to a cream color and shift 10px.
    //     // We do this by setting the appropriate style properties.
    //     this.style.position = "relative";
    //     this.style.left = "10px";
    //     this.style.backgroundColor = "#ffe";
    //   };
    //   divs[i].onmouseout = function(e) {
    //     this.style.left = "0px";
    //     this.style.backgroundColor = "#fff";
    //   };
    // }

