<?php
$dbname = $user = $pass = 'lec17';

try {
  $dbconn = new PDO('mysql:host=localhost;dbname='.$dbname, $user, $pass);
}
catch (Exception $e) {
  echo "Error: ";
  echo $e->getMessage();
}
?>
<!doctype html>
<html>
<head>
  <title>Colors!</title>
</head>
<body>
  <h1>Colors:</h1>
  <ul>
  <?php
  if (isset($_GET['color'])) {
    $stmt = $dbconn->prepare("SELECT * FROM lec17 WHERE colorname = :colorname");
    $stmt->execute(array(':colorname' => $_GET['color']));  
  }
  else {
    $stmt = $dbconn->query("SELECT * FROM lec17");
  } 
  foreach ($stmt as $row) {
    echo '<li>' . htmlentities($row['colorname']) . ' - ' . htmlentities($row['hex']) . '</li>';
  }
  ?>
  </ul>
</body>
</html>
