<?php
try {
  $dbname = $user = $pass = 'lec20';
  $dbconn = new PDO('mysql:host=localhost;dbname='.$dbname, $user, $pass);
}
catch (Exception $e) {
  echo "Error: " . $e->getMessage();
}

if (isset($_GET['post']) && $_GET['post'] == 'Submit' && !empty($_GET['txt'])) {
  // Try passing in the following...
  // '); DELETE FROM lec20;--
  // "And I hope you've learned to sanitize your database inputs."
  $res = $dbconn->exec("INSERT INTO lec20 (txt) VALUES ('{$_GET['txt']}')");
}
?>
<!doctype html>
<html>
<head>
  <title>Lecture #20 - INSECURE</title>
</head>
<body>
  <form method="GET" action="insecure.php">
    <input name="txt" type="text"/>
    <input name="post" type="submit" value="Submit"/>
  </form>
  <?php 
  $stmt = $dbconn->query('SELECT txt FROM lec20');
  foreach ($stmt as $row): ?>
  <p><?php 

  // Vulnerable to XSS. Try the following input:
  // <script>alert('xss');</script>
  // We could also redirect to another site and pass information about the user
  // (say, using document.cookie, if we wanted to lift their session key)
  echo $row['txt']; 

  ?></p>
  <?php endforeach; ?>
</body>
</html>
