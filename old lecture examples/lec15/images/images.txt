<?php
  // Set the size of the cavnas
  // imagecreate(int $width, int $height)
  $img = imagecreate(400, 200);
  
  // Sets the colors we can use
  // imagecolorallocate(resource $image , int $red , int $green , int $blue )
  $black = imagecolorallocate($img, 0, 0, 0);
  $white = imagecolorallocate($img, 255, 255, 255);
  $red   = imagecolorallocate($img, 255, 0, 0);
  $green = imagecolorallocate($img, 0, 255, 0);
  $blue  = imagecolorallocate($img, 0, 0, 255);
  
  // Create a empty (unfilled) rectangle
  // imagerectangle ( resource $image , int $x1 , int $y1 , int $x2 , int $y2 , int $color )
  imagerectangle($img, 10, 10, 390, 190, $white);
  
  // Create a filled rectangle
  // imagefilledrectangle ( resource $image , int $x1 , int $y1 , int $x2 , int $y2 , int $color )
  imagefilledrectangle($img, 20, 20, 60, 60, $red);
  
  // Create a filled eclipse
  // imagefilledellipse ( resource $image , int $x-coordinate-of-the-center , int $y-coordinate-of-the-center , int $width , int $height , int $color )
  imagefilledellipse($img, 190, 100, 80, 80, $blue);
  imagefilledellipse($img, 350, 160, 70, 40, $green);
  
  // Set the content type for the header
  header ("Content-type: image/jpeg");
  
  // Creates a jpeg of the given resourse
  // imagejpeg ( resource $image [, string $filename [, int $quality ]] )
  imagejpeg($img);
  
  // Clears memory used by the imagecreate functions
  imagedestroy($img);
